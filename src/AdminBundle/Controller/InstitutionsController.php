<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\InstitutionFormType;
use AppBundle\Entity\Institution;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/admin/institutions")
 */
class InstitutionsController extends Controller
{
    /**
     * @Route("", name="admin_institutions")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $institutions = $em->getRepository('AppBundle:Institution')->findAll();

        return $this->render('@Admin/institutions/index.html.twig', array(
            'institutions' => $institutions
        ));
    }

    /**
     * @Route("/new", name="admin_institutions_new")
     * @param Request $request
     * @return Response
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $institution = new Institution();

        $form = $this->createForm(InstitutionFormType::class, $institution);
        $form->add('save', SubmitType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($institution);
            $em->flush();

            return $this->redirectToRoute('admin_institutions');
        }

        return $this->render('@Admin/institutions/detail.html.twig', array(
            'form' => $form->createView(),
            'institution' => $institution
        ));
    }

    /**
     * @Route("/{institution}/edit", name="admin_institutions_edit")
     * @param Request $request
     * @param Institution $institution
     * @return Response
     */
    public function institutionEditAction(Request $request, Institution $institution)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(InstitutionFormType::class, $institution);
        $form->add('save', SubmitType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($institution);
            $em->flush();

            return $this->redirectToRoute('admin_institutions');
        }

        return $this->render('@Admin/institutions/detail.html.twig', array(
            'form' => $form->createView(),
            'institution' => $institution
        ));
    }

    /**
     * @Route("/{institution}/delete", name="admin_institutions_delete")
     * @param Request $request
     * @param Institution $institution
     * @return Response
     */
    public function deleteAction(Request $request, Institution $institution)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($institution);
        $em->flush();

        return $this->redirectToRoute('admin_institutions');
    }
}
