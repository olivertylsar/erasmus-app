<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\UniversityPersonFormType;
use AppBundle\Entity\InstitutionPerson;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/admin/university-persons")
 */
class UniversityPersonsController extends Controller
{
    /**
     * @Route("", name="admin_university_persons")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $upt = $em->getRepository('AppBundle:Institution')->find(1);
        $universityPersons = $em->getRepository('AppBundle:InstitutionPerson')->findBy(array(
            'institution' => $upt
        ));

        return $this->render('@Admin/university-persons/index.html.twig', array(
            'universityPersons' => $universityPersons
        ));
    }

    /**
     * @Route("/new", name="admin_university_persons_new")
     * @param Request $request
     * @return Response
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $institutionPerson = new InstitutionPerson();

        $form = $this->createForm(UniversityPersonFormType::class, $institutionPerson);
        $form->add('save', SubmitType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $upt = $em->getRepository('AppBundle:Institution')->find(1);
            $institutionPerson->setInstitution($upt);
            $em->persist($institutionPerson);
            $em->flush();

            return $this->redirectToRoute('admin_university_persons');
        }

        return $this->render('@Admin/university-persons/detail.html.twig', array(
            'form' => $form->createView(),
            'institutionPerson' => $institutionPerson
        ));
    }

    /**
     * @Route("/{institutionPerson}/edit", name="admin_university_persons_edit")
     * @param Request $request
     * @param InstitutionPerson $institutionPerson
     * @return Response
     */
    public function institutionEditAction(Request $request, InstitutionPerson $institutionPerson)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(UniversityPersonFormType::class, $institutionPerson);
        $form->add('save', SubmitType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($institutionPerson);
            $em->flush();

            return $this->redirectToRoute('admin_university_persons');
        }

        return $this->render('@Admin/university-persons/detail.html.twig', array(
            'form' => $form->createView(),
            'institutionPerson' => $institutionPerson
        ));
    }

    /**
     * @Route("/{institutionPerson}/delete", name="admin_university_persons_delete")
     * @param Request $request
     * @param InstitutionPerson $institutionPerson
     * @return Response
     */
    public function deleteAction(Request $request, InstitutionPerson $institutionPerson)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($institutionPerson);
        $em->flush();

        return $this->redirectToRoute('admin_university_persons');
    }
}
