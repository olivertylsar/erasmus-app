<?php

namespace AdminBundle\Form;

use AppBundle\Entity\Country;
use AppBundle\Entity\Institution;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InstitutionFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('country', EntityType::class,array(
                'class' => Country::class,
                'choice_label' => 'countryName',
                'placeholder' => '---'
            ))
            ->add('address', TextType::class)
            ->add('erasmusCode', TextType::class)
            ->add('website', TextType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Institution::class
        ));
    }

    public function getBlockPrefix()
    {
        return 'admin_bundle_institution_form_type';
    }
}
