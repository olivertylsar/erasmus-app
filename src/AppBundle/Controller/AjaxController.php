<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AjaxController extends Controller
{
    /**
     * @Route("/institution-info-ajax", name="learning_agreement_institution_info_ajax")
     * @return JsonResponse
     */
    public function ajaxInstitutionInfoAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $institution = $em->getRepository('AppBundle:Institution')->find($request->get('institution'));

        $result = array();

        if (!$institution) {
            $result['ok'] = false;
        }
        else {
            $result['ok'] = true;
            $result['country'] = $institution->getCountry()->getId();
            $result['address'] = $institution->getAddress();
            $result['erasmusCode'] = $institution->getErasmusCode();
        }

        return new JsonResponse($result);
    }

    /**
     * @Route("/institution-person-ajax", name="learning_agreement_institution_person_ajax")
     * @return JsonResponse
     */
    public function ajaxInstitutionPersonAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $institutionPerson = $em->getRepository('AppBundle:InstitutionPerson')->find($request->get('institutionPerson'));

        $result = array();

        if (!$institutionPerson) {
            $result['ok'] = false;
        }
        else {
            $result['ok'] = true;
            $result['position'] = $institutionPerson->getPosition();
            $result['email'] = $institutionPerson->getEmail();
            $result['phoneNumber'] = $institutionPerson->getPhoneNumber();
        }

        return new JsonResponse($result);
    }
}
