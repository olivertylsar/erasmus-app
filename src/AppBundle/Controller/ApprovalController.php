<?php

namespace AppBundle\Controller;

use AppBundle\Entity\LearningAgreement;
use AppBundle\Entity\Signature;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @Route("/learning-agreements/{learningAgreement}/approval")
 * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_RECEIVING_ACCOUNT')")
 */
class ApprovalController extends Controller
{
    /**
     * @Route("/sending", name="learning_agreement_approval_sending")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @param LearningAgreement $learningAgreement
     * @return Response
     */
    public function approvalSendingAction(Request $request, LearningAgreement $learningAgreement)
    {
        $beforeMobility = $learningAgreement->getBeforeMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');

        if (!$authorizationChecker->isGranted('view', $beforeMobility)) {
            throw new AccessDeniedHttpException();
        }

        $em = $this->getDoctrine()->getManager();

        $fs = new Filesystem();

        $studentSignature = $beforeMobility->getStudentSignature();
        if ($studentSignature) {
            $signaturePath = "images/signatures/" . $learningAgreement->getId() . '/' . $studentSignature->getFilename() . '.png';
            $studentSignature = $fs->exists($signaturePath) ? $signaturePath : null;
        }

        $sendingSignature = $beforeMobility->getSendingSignature();
        if ($sendingSignature) {
            $signaturePath = "images/signatures/" . $learningAgreement->getId() . '/' . $sendingSignature->getFilename() . '.png';
            $sendingSignature = $fs->exists($signaturePath) ? $signaturePath : null;
        }

        $receivingSignature = $beforeMobility->getReceivingSignature();
        if ($receivingSignature) {
            $signaturePath = "images/signatures/" . $learningAgreement->getId() . '/' . $receivingSignature->getFilename() . '.png';
            $receivingSignature = $fs->exists($signaturePath) ? $signaturePath : null;
        }

        $form = $this->createFormBuilder()
            ->add('save', SubmitType::class, array(
                'label' => 'Approve',
            ))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($sendingSignature) {
                $beforeMobility->setIsSignedBySending(true);
            }

            $receivingResponsiblePerson = $learningAgreement->getReceivingInstitution()->getResponsiblePerson()->getInstitutionPerson();
            $email = $receivingResponsiblePerson->getEmail();
            $accountType = $em->getRepository('AppBundle:AccountType')->findOneBy(array(
                'accountTypeName' => 'Receiving University Account'
            ));

            $existingAccount = $em->getRepository('AppBundle:User')->findOneBy(array('email' => $email));
            $password = null;

            if (!$existingAccount) {
                $password = base64_encode(random_bytes(10));

                $receivingNewUser = new User();
                $receivingNewUser
                    ->setFirstName($receivingResponsiblePerson->getName())
                    ->setUsername($email)
                    ->setEmail($email)
                    ->setEmailCanonical($email)
                    ->setPlainPassword($password)
                    ->setRoles(array(
                        'ROLE_RECEIVING_ACCOUNT'
                    ))
                    ->setEnabled(true)
                    ->setAccountType($accountType);

                $em->persist($receivingNewUser);
            }

            $em->flush();

            $subject = $existingAccount ? 'UPT Erasmus Online Application' : 'UPT Erasmus Online Application - Invitation email';

            $email = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom($this->getParameter('mailer_user'))
                ->setTo($receivingResponsiblePerson->getEmail())
                ->addReplyTo($this->getParameter('mailer_user'))
                ->setBody($this->renderView(':email:sendemail.html.twig',array(
                    'existingAccount' => $existingAccount,
                    'learningAgreement' => $learningAgreement,
                    'responsiblePerson' => $receivingResponsiblePerson,
                    'password' => $password
                )),'text/html');

            $this->get('mailer')->send($email);

            $clickedButton = $form->getClickedButton()->getName();

            if ($clickedButton != 'save' && $clickedButton != null) {
                $redirectService = $this->get('redirect_service');
                return $redirectService->buttonRedirectAction($learningAgreement, $clickedButton, 'learning_agreement_responsible_persons', null);
            }

            return $this->redirectToRoute('learning_agreements');
        }

        return $this->render('learning-agreements/approval_sending.html.twig', array(
            'form' => $form->createView(),
            'learningAgreement' => $learningAgreement,
            'studentSignature' => $studentSignature,
            'sendingSignature' => $sendingSignature,
            'receivingSignature' => $receivingSignature
        ));
    }

    /**
     * @Route("/sending/signature-digital", name="learning_agreement_approval_sending_signature_digital")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @param LearningAgreement $learningAgreement
     * @return Response
     */
    public function signatureDigitalAction(Request $request, LearningAgreement $learningAgreement)
    {
        $beforeMobility = $learningAgreement->getBeforeMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');

        if (!$authorizationChecker->isGranted('edit', $beforeMobility)) {
            throw new AccessDeniedHttpException();
        }

        $em = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder()
            ->add('signature', HiddenType::class, array(
                'data' => 'signature'
            ))
            ->add('save', SubmitType::class, array(
                'label' => 'Save signature'
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $signatureDataUri = $form->get('signature')->getData();
            if (!($signatureDataUri === 'signature')) { //save only if signature field is not empty
                $encodedImage = explode(",", $signatureDataUri)[1];
                $decodedImage = base64_decode($encodedImage);

                $fs = new Filesystem();
                $dirToSave = "images/signatures/" . $learningAgreement->getId();

                if (!$fs->exists($dirToSave)) {
                    $fs->mkdir($dirToSave);
                }

                $defaultSendingSignature = $beforeMobility->getSendingSignature();

                $uniqueFileName = $this->generateUniqueFileName();
                file_put_contents($dirToSave . '/' . $uniqueFileName . '.png', $decodedImage);

                $signature = new Signature();
                $signature
                    ->setRole('sending')
                    ->setAgreementSection($beforeMobility)
                    ->setSignedAt(new \DateTime('now', new \DateTimeZone('Europe/Lisbon')))
                    ->setFilename($uniqueFileName)
                    ->setIsDigital(true);

                $em->persist($signature);

                if ($defaultSendingSignature) {
                    $defaultFile = "images/signatures/" . $learningAgreement->getId() . '/' . $defaultSendingSignature->getFilename() . '.png';
                    $em->remove($defaultSendingSignature);
                    $fs->remove($defaultFile);
                }

                $em->flush();
            }

            return $this->redirectToRoute('learning_agreement_approval_sending', array(
                'learningAgreement' => $learningAgreement->getId()
            ));
        }

        return $this->render('learning-agreements/signature_digital.html.twig', array(
            'form' => $form->createView(),
            'learningAgreement' => $learningAgreement
        ));
    }

    /**
     * @Route("/sending/signature-upload", name="learning_agreement_approval_sending_signature_upload")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @param LearningAgreement $learningAgreement
     * @return Response
     */
    public function sendingSignatureUploadAction(Request $request, LearningAgreement $learningAgreement)
    {
        $beforeMobility = $learningAgreement->getBeforeMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');

        if (!$authorizationChecker->isGranted('edit', $beforeMobility)) {
            throw new AccessDeniedHttpException();
        }

        $em = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder()
            ->add('signature', FileType::class, array(
            ))
            ->add('save', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $uploadedFile */
            $uploadedFile = $form->get('signature')->getData();

            $fs = new Filesystem();
            $dirToSave = "images/signatures/" . $learningAgreement->getId();

            if (!$fs->exists($dirToSave)) {
                $fs->mkdir($dirToSave);
            }

            $defaultSendingSignature = $beforeMobility->getSendingSignature();

            $uniqueFileName = $this->generateUniqueFileName();
            $fileName = $uniqueFileName . '.png';
            $uploadedFile->move($dirToSave, $fileName);

            $signature = new Signature();
            $signature
                ->setRole('sending')
                ->setAgreementSection($beforeMobility)
                ->setSignedAt(new \DateTime('now', new \DateTimeZone('Europe/Lisbon')))
                ->setFilename($uniqueFileName)
                ->setIsDigital(false);

            $em->persist($signature);

            if ($defaultSendingSignature) {
                $defaultFile = "images/signatures/" . $learningAgreement->getId() . '/' . $defaultSendingSignature->getFilename() . '.png';
                $em->remove($defaultSendingSignature);
                $fs->remove($defaultFile);
            }

            $em->flush();

            return $this->redirectToRoute('learning_agreement_approval_sending', array(
                'learningAgreement' => $learningAgreement->getId()
            ));
        }

        return $this->render('learning-agreements/signature_upload.html.twig', array(
            'form' => $form->createView(),
            'learningAgreement' => $learningAgreement
        ));
    }

    /**
     * @Route("/receiving", name="learning_agreement_approval_receiving")
     * @Security("has_role('ROLE_RECEIVING_ACCOUNT')")
     * @param Request $request
     * @param LearningAgreement $learningAgreement
     * @return Response
     */
    public function approvalReceivingAction(Request $request, LearningAgreement $learningAgreement)
    {
        $beforeMobility = $learningAgreement->getBeforeMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');

        if (!$authorizationChecker->isGranted('view', $beforeMobility)) {
            throw new AccessDeniedHttpException();
        }

        $em = $this->getDoctrine()->getManager();
        $fs = new Filesystem();

        $studentSignature = $beforeMobility->getStudentSignature();
        if ($studentSignature) {
            $signaturePath = "images/signatures/" . $learningAgreement->getId() . '/' . $studentSignature->getFilename() . '.png';
            $studentSignature = $fs->exists($signaturePath) ? $signaturePath : null;
        }

        $sendingSignature = $beforeMobility->getSendingSignature();
        if ($sendingSignature) {
            $signaturePath = "images/signatures/" . $learningAgreement->getId() . '/' . $sendingSignature->getFilename() . '.png';
            $sendingSignature = $fs->exists($signaturePath) ? $signaturePath : null;
        }

        $receivingSignature = $beforeMobility->getReceivingSignature();
        if ($receivingSignature) {
            $signaturePath = "images/signatures/" . $learningAgreement->getId() . '/' . $receivingSignature->getFilename() . '.png';
            $receivingSignature = $fs->exists($signaturePath) ? $signaturePath : null;
        }

        $form = $this->createFormBuilder()
            ->add('save', SubmitType::class, array(
                'label' => 'Approve',
            ))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($sendingSignature && $receivingSignature) {
                $beforeMobility->setIsSignedByReceiving(true);
            }

            $em->flush();

            $clickedButton = $form->getClickedButton()->getName();

            if ($clickedButton != 'save' && $clickedButton != null) {
                $redirectService = $this->get('redirect_service');
                return $redirectService->buttonRedirectAction($learningAgreement, $clickedButton, 'learning_agreement_responsible_persons', null);
            }

            return $this->redirectToRoute('learning_agreements');
        }

        return $this->render('learning-agreements/approval_receiving.html.twig', array(
            'form' => $form->createView(),
            'learningAgreement' => $learningAgreement,
            'studentSignature' => $studentSignature,
            'sendingSignature' => $sendingSignature,
            'receivingSignature' => $receivingSignature
        ));
    }

    /**
     * @Route("/receiving/signature-digital", name="learning_agreement_approval_receiving_signature_digital")
     * @Security("has_role('ROLE_RECEIVING_ACCOUNT')")
     * @param Request $request
     * @param LearningAgreement $learningAgreement
     * @return Response
     */
    public function receivingSignatureDigitalAction(Request $request, LearningAgreement $learningAgreement)
    {
        $beforeMobility = $learningAgreement->getBeforeMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');

        if (!$authorizationChecker->isGranted('sign', $beforeMobility)) {
            throw new AccessDeniedHttpException();
        }

        $em = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder()
            ->add('signature', HiddenType::class, array(
                'data' => 'signature'
            ))
            ->add('save', SubmitType::class, array(
                'label' => 'Save signature'
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $signatureDataUri = $form->get('signature')->getData();
            if (!($signatureDataUri === 'signature')) { //save only if signature field is not empty
                $encodedImage = explode(",", $signatureDataUri)[1];
                $decodedImage = base64_decode($encodedImage);

                $fs = new Filesystem();
                $dirToSave = "images/signatures/" . $learningAgreement->getId();

                if (!$fs->exists($dirToSave)) {
                    $fs->mkdir($dirToSave);
                }

                $defaultReceivingSignature = $beforeMobility->getReceivingSignature();

                $uniqueFileName = $this->generateUniqueFileName();
                file_put_contents($dirToSave . '/' . $uniqueFileName . '.png', $decodedImage);

                $signature = new Signature();
                $signature
                    ->setRole('receiving')
                    ->setAgreementSection($beforeMobility)
                    ->setSignedAt(new \DateTime('now', new \DateTimeZone('Europe/Lisbon')))
                    ->setFilename($uniqueFileName)
                    ->setIsDigital(true);

                $em->persist($signature);

                if ($defaultReceivingSignature) {
                    $defaultFile = "images/signatures/" . $learningAgreement->getId() . '/' . $defaultReceivingSignature->getFilename() . '.png';
                    $em->remove($defaultReceivingSignature);
                    $fs->remove($defaultFile);
                }

                $em->flush();
            }

            return $this->redirectToRoute('learning_agreement_approval_receiving', array(
                'learningAgreement' => $learningAgreement->getId()
            ));
        }

        return $this->render('learning-agreements/signature_digital.html.twig', array(
            'form' => $form->createView(),
            'learningAgreement' => $learningAgreement
        ));
    }

    /**
     * @Route("/receiving/signature-upload", name="learning_agreement_approval_receiving_signature_upload")
     * @Security("has_role('ROLE_RECEIVING_ACCOUNT')")
     * @param Request $request
     * @param LearningAgreement $learningAgreement
     * @return Response
     */
    public function receivingSignatureUploadAction(Request $request, LearningAgreement $learningAgreement)
    {
        $beforeMobility = $learningAgreement->getBeforeMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');

        if (!$authorizationChecker->isGranted('sign', $beforeMobility)) {
            throw new AccessDeniedHttpException();
        }

        $em = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder()
            ->add('signature', FileType::class, array(
            ))
            ->add('save', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $uploadedFile */
            $uploadedFile = $form->get('signature')->getData();

            $fs = new Filesystem();
            $dirToSave = "images/signatures/" . $learningAgreement->getId();

            if (!$fs->exists($dirToSave)) {
                $fs->mkdir($dirToSave);
            }

            $defaultReceivingSignature = $beforeMobility->getReceivingSignature();

            $uniqueFileName = $this->generateUniqueFileName();
            $fileName = $uniqueFileName . '.png';
            $uploadedFile->move($dirToSave, $fileName);

            $signature = new Signature();
            $signature
                ->setRole('receiving')
                ->setAgreementSection($beforeMobility)
                ->setSignedAt(new \DateTime('now', new \DateTimeZone('Europe/Lisbon')))
                ->setFilename($uniqueFileName)
                ->setIsDigital(false);

            $em->persist($signature);

            if ($defaultReceivingSignature) {
                $defaultFile = "images/signatures/" . $learningAgreement->getId() . '/' . $defaultReceivingSignature->getFilename() . '.png';
                $em->remove($defaultReceivingSignature);
                $fs->remove($defaultFile);
            }

            $em->flush();

            return $this->redirectToRoute('learning_agreement_approval_receiving', array(
                'learningAgreement' => $learningAgreement->getId()
            ));
        }

        return $this->render('learning-agreements/signature_upload.html.twig', array(
            'form' => $form->createView(),
            'learningAgreement' => $learningAgreement
        ));
    }

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        return md5(uniqid());
    }
}