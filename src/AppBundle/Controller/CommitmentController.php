<?php

namespace AppBundle\Controller;

use AppBundle\Entity\LearningAgreement;
use AppBundle\Entity\Signature;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @Route("/learning-agreements/{learningAgreement}/commitment")
 * @Security("has_role('ROLE_STUDENT')")
 */
class CommitmentController extends Controller
{
    /**
     * @Route("", name="learning_agreement_commitment")
     * @param Request $request
     * @param LearningAgreement $learningAgreement
     * @return Response
     */
    public function commitmentAction(Request $request, LearningAgreement $learningAgreement)
    {
        $beforeMobility = $learningAgreement->getBeforeMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');

        if (!$authorizationChecker->isGranted('view', $beforeMobility)) {
            throw new AccessDeniedHttpException();
        }

        $redirectService = $this->get('redirect_service');
        $hasAllFields = $redirectService->checkRequiredFields($learningAgreement);

        $em = $this->getDoctrine()->getManager();
        $fs = new Filesystem();

        $studentSignature = $beforeMobility->getStudentSignature();
        if ($studentSignature) {
            $signaturePath = "images/signatures/" . $learningAgreement->getId() . '/' . $studentSignature->getFilename() . '.png';
            $studentSignature = $fs->exists($signaturePath) ? $signaturePath : null;
        }

        $sendingSignature = $beforeMobility->getSendingSignature();
        if ($sendingSignature) {
            $signaturePath = "images/signatures/" . $learningAgreement->getId() . '/' . $sendingSignature->getFilename() . '.png';
            $sendingSignature = $fs->exists($signaturePath) ? $signaturePath : null;
        }

        $receivingSignature = $beforeMobility->getReceivingSignature();
        if ($receivingSignature) {
            $signaturePath = "images/signatures/" . $learningAgreement->getId() . '/' . $receivingSignature->getFilename() . '.png';
            $receivingSignature = $fs->exists($signaturePath) ? $signaturePath : null;
        }

        $form = $this->createFormBuilder()
            ->add('save', SubmitType::class, array(
                'label' => 'Commit',
            ))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($studentSignature) {
                $beforeMobility->setIsSignedByStudent(true);
            }

            $em->flush();

            $clickedButton = $form->getClickedButton()->getName();

            if ($clickedButton != 'save' && $clickedButton != null) {
                return $redirectService->buttonRedirectAction($learningAgreement, $clickedButton, 'learning_agreement_responsible_persons', null);
            }

            return $this->redirectToRoute('learning_agreements');
        }

        return $this->render('learning-agreements/commitment.html.twig', array(
            'form' => $form->createView(),
            'learningAgreement' => $learningAgreement,
            'studentSignature' => $studentSignature,
            'sendingSignature' => $sendingSignature,
            'receivingSignature' => $receivingSignature,
            'hasAllFields' => $hasAllFields
        ));
    }

    /**
     * @Route("/signature-digital", name="learning_agreement_commitment_signature_digital")
     * @param Request $request
     * @param LearningAgreement $learningAgreement
     * @return Response
     */
    public function signatureDigitalAction(Request $request, LearningAgreement $learningAgreement)
    {
        $beforeMobility = $learningAgreement->getBeforeMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');

        if (!$authorizationChecker->isGranted('edit', $beforeMobility)) {
            throw new AccessDeniedHttpException();
        }

        $em = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder()
            ->add('signature', HiddenType::class, array(
                'data' => 'signature'
            ))
            ->add('save', SubmitType::class, array(
                'label' => 'Save signature'
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $signatureDataUri = $form->get('signature')->getData();
            if (!($signatureDataUri === 'signature')) { //save only if signature field is not empty
                $encodedImage = explode(",", $signatureDataUri)[1];
                $decodedImage = base64_decode($encodedImage);

                $fs = new Filesystem();
                $dirToSave = "images/signatures/" . $learningAgreement->getId();

                if (!$fs->exists($dirToSave)) {
                    $fs->mkdir($dirToSave);
                }

                $defaultStudentSignature = $learningAgreement->getBeforeMobilitySection()->getStudentSignature();

                $uniqueFileName = $this->generateUniqueFileName();
                file_put_contents($dirToSave . '/' . $uniqueFileName . '.png', $decodedImage);

                $signature = new Signature();
                $signature
                    ->setRole('student')
                    ->setAgreementSection($learningAgreement->getBeforeMobilitySection())
                    ->setSignedAt(new \DateTime('now', new \DateTimeZone('Europe/Lisbon')))
                    ->setFilename($uniqueFileName)
                    ->setIsDigital(true);

                $em->persist($signature);

                if ($defaultStudentSignature) {
                    $defaultFile = "images/signatures/" . $learningAgreement->getId() . '/' . $defaultStudentSignature->getFilename() . '.png';
                    $em->remove($defaultStudentSignature);
                    $fs->remove($defaultFile);
                }

                $em->flush();
            }

            return $this->redirectToRoute('learning_agreement_commitment', array(
                'learningAgreement' => $learningAgreement->getId()
            ));
        }

        return $this->render('learning-agreements/signature_digital.html.twig', array(
            'form' => $form->createView(),
            'learningAgreement' => $learningAgreement
        ));
    }

    /**
     * @Route("/signature-upload", name="learning_agreement_commitment_signature_upload")
     * @param Request $request
     * @param LearningAgreement $learningAgreement
     * @return Response
     */
    public function signatureUploadAction(Request $request, LearningAgreement $learningAgreement)
    {
        $beforeMobility = $learningAgreement->getBeforeMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');

        if (!$authorizationChecker->isGranted('edit', $beforeMobility)) {
            throw new AccessDeniedHttpException();
        }

        $em = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder()
            ->add('signature', FileType::class, array(
            ))
            ->add('save', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $uploadedFile */
            $uploadedFile = $form->get('signature')->getData();

            $fs = new Filesystem();
            $dirToSave = "images/signatures/" . $learningAgreement->getId();

            if (!$fs->exists($dirToSave)) {
                $fs->mkdir($dirToSave);
            }

            $defaultStudentSignature = $learningAgreement->getBeforeMobilitySection()->getStudentSignature();

            $uniqueFileName = $this->generateUniqueFileName();
            $fileName = $uniqueFileName . '.png';
            $uploadedFile->move($dirToSave, $fileName);

            $signature = new Signature();
            $signature
                ->setRole('student')
                ->setAgreementSection($learningAgreement->getBeforeMobilitySection())
                ->setSignedAt(new \DateTime('now', new \DateTimeZone('Europe/Lisbon')))
                ->setFilename($uniqueFileName)
                ->setIsDigital(false);

            $em->persist($signature);

            if ($defaultStudentSignature) {
                $defaultFile = "images/signatures/" . $learningAgreement->getId() . '/' . $defaultStudentSignature->getFilename() . '.png';
                $em->remove($defaultStudentSignature);
                $fs->remove($defaultFile);
            }

            $em->flush();

            return $this->redirectToRoute('learning_agreement_commitment', array(
                'learningAgreement' => $learningAgreement->getId()
            ));
        }

        return $this->render('learning-agreements/signature_upload.html.twig', array(
            'form' => $form->createView(),
            'learningAgreement' => $learningAgreement
        ));
    }

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        return md5(uniqid());
    }
}