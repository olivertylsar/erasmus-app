<?php

namespace AppBundle\Controller;

use AppBundle\Entity\LearningAgreement;
use AppBundle\Entity\Signature;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @Route("/learning-agreements/{learningAgreement}/during-mobility/approval")
 * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_RECEIVING_ACCOUNT')")
 */
class DuringMobilityApprovalController extends Controller
{
    /**
     * @Route("/sending", name="learning_agreement_during_mobility_approval_sending")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @param LearningAgreement $learningAgreement
     * @return Response
     */
    public function approvalSendingAction(Request $request, LearningAgreement $learningAgreement)
    {
        $duringMobility = $learningAgreement->getDuringMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');
        if (!$authorizationChecker->isGranted('view', $duringMobility)) {
            throw new AccessDeniedHttpException();
        }

        $em = $this->getDoctrine()->getManager();

        $fs = new Filesystem();

        $duringMobility = $learningAgreement->getDuringMobilitySection();
        $studentSignature = $duringMobility->getStudentSignature();
        if ($studentSignature) {
            $signaturePath = "images/signatures/" . $learningAgreement->getId() . '/' . $studentSignature->getFilename() . '.png';
            $studentSignature = $fs->exists($signaturePath) ? $signaturePath : null;
        }

        $sendingSignature = $duringMobility->getSendingSignature();
        if ($sendingSignature) {
            $signaturePath = "images/signatures/" . $learningAgreement->getId() . '/' . $sendingSignature->getFilename() . '.png';
            $sendingSignature = $fs->exists($signaturePath) ? $signaturePath : null;
        }

        $receivingSignature = $duringMobility->getReceivingSignature();
        if ($receivingSignature) {
            $signaturePath = "images/signatures/" . $learningAgreement->getId() . '/' . $receivingSignature->getFilename() . '.png';
            $receivingSignature = $fs->exists($signaturePath) ? $signaturePath : null;
        }

        $form = $this->createFormBuilder()
            ->add('save', SubmitType::class, array(
                'label' => 'Approve',
            ))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($sendingSignature) {
                $duringMobility->setIsSignedBySending(true);
            }

            $receivingResponsiblePerson = $learningAgreement->getReceivingInstitution()->getResponsiblePerson()->getInstitutionPerson();

            $em->flush();

            $subject = 'UPT Erasmus Online Application - During mobility changes';

            $email = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom($this->getParameter('mailer_user'))
                ->setTo($receivingResponsiblePerson->getEmail())
                ->addReplyTo($this->getParameter('mailer_user'))
                ->setBody($this->renderView(':email:sendemail_during_mobility.html.twig',array(
                    'learningAgreement' => $learningAgreement,
                    'responsiblePerson' => $receivingResponsiblePerson
                )),'text/html');

            $this->get('mailer')->send($email);

            $clickedButton = $form->getClickedButton()->getName();

            if ($clickedButton != 'save' && $clickedButton != null) {
                $redirectService = $this->get('redirect_service');
                return $redirectService->buttonRedirectAction($learningAgreement, $clickedButton, 'learning_agreement_responsible_persons', null);
            }

            return $this->redirectToRoute('learning_agreements');
        }

        return $this->render('learning-agreements/during-mobility/approval_sending.html.twig', array(
            'form' => $form->createView(),
            'learningAgreement' => $learningAgreement,
            'studentSignature' => $studentSignature,
            'sendingSignature' => $sendingSignature,
            'receivingSignature' => $receivingSignature
        ));
    }

    /**
     * @Route("/sending/signature-digital", name="learning_agreement_during_mobility_approval_sending_signature_digital")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @param LearningAgreement $learningAgreement
     * @return Response
     */
    public function signatureDigitalAction(Request $request, LearningAgreement $learningAgreement)
    {
        $duringMobility = $learningAgreement->getDuringMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');
        if (!$authorizationChecker->isGranted('sign', $duringMobility)) {
            throw new AccessDeniedHttpException();
        }

        $em = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder()
            ->add('signature', HiddenType::class, array(
                'data' => 'signature'
            ))
            ->add('save', SubmitType::class, array(
                'label' => 'Save signature'
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $signatureDataUri = $form->get('signature')->getData();
            if (!($signatureDataUri === 'signature')) { //save only if signature field is not empty
                $encodedImage = explode(",", $signatureDataUri)[1];
                $decodedImage = base64_decode($encodedImage);

                $fs = new Filesystem();
                $dirToSave = "images/signatures/" . $learningAgreement->getId();

                if (!$fs->exists($dirToSave)) {
                    $fs->mkdir($dirToSave);
                }

                $defaultSendingSignature = $duringMobility->getSendingSignature();

                $uniqueFileName = $this->generateUniqueFileName();
                file_put_contents($dirToSave . '/' . $uniqueFileName . '.png', $decodedImage);

                $signature = new Signature();
                $signature
                    ->setRole('sending')
                    ->setAgreementSection($duringMobility)
                    ->setSignedAt(new \DateTime('now', new \DateTimeZone('Europe/Lisbon')))
                    ->setFilename($uniqueFileName)
                    ->setIsDigital(true);

                $em->persist($signature);

                if ($defaultSendingSignature) {
                    $defaultFile = "images/signatures/" . $learningAgreement->getId() . '/' . $defaultSendingSignature->getFilename() . '.png';
                    $em->remove($defaultSendingSignature);
                    $fs->remove($defaultFile);
                }

                $em->flush();
            }

            return $this->redirectToRoute('learning_agreement_during_mobility_approval_sending', array(
                'learningAgreement' => $learningAgreement->getId()
            ));
        }

        return $this->render('learning-agreements/during-mobility/signature_digital.html.twig', array(
            'form' => $form->createView(),
            'learningAgreement' => $learningAgreement
        ));
    }

    /**
     * @Route("/sending/signature-upload", name="learning_agreement_during_mobility_approval_sending_signature_upload")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @param LearningAgreement $learningAgreement
     * @return Response
     */
    public function sendingSignatureUploadAction(Request $request, LearningAgreement $learningAgreement)
    {
        $duringMobility = $learningAgreement->getDuringMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');
        if (!$authorizationChecker->isGranted('sign', $duringMobility)) {
            throw new AccessDeniedHttpException();
        }

        $em = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder()
            ->add('signature', FileType::class, array(
            ))
            ->add('save', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $uploadedFile */
            $uploadedFile = $form->get('signature')->getData();

            $fs = new Filesystem();
            $dirToSave = "images/signatures/" . $learningAgreement->getId();

            if (!$fs->exists($dirToSave)) {
                $fs->mkdir($dirToSave);
            }

            $defaultSendingSignature = $duringMobility->getSendingSignature();

            $uniqueFileName = $this->generateUniqueFileName();
            $fileName = $uniqueFileName . '.png';
            $uploadedFile->move($dirToSave, $fileName);

            $signature = new Signature();
            $signature
                ->setRole('sending')
                ->setAgreementSection($duringMobility)
                ->setSignedAt(new \DateTime('now', new \DateTimeZone('Europe/Lisbon')))
                ->setFilename($uniqueFileName)
                ->setIsDigital(false);

            $em->persist($signature);

            if ($defaultSendingSignature) {
                $defaultFile = "images/signatures/" . $learningAgreement->getId() . '/' . $defaultSendingSignature->getFilename() . '.png';
                $em->remove($defaultSendingSignature);
                $fs->remove($defaultFile);
            }

            $em->flush();

            return $this->redirectToRoute('learning_agreement_during_mobility_approval_sending', array(
                'learningAgreement' => $learningAgreement->getId()
            ));
        }

        return $this->render('learning-agreements/during-mobility/signature_upload.html.twig', array(
            'form' => $form->createView(),
            'learningAgreement' => $learningAgreement
        ));
    }

    /**
     * @Route("/receiving", name="learning_agreement_during_mobility_approval_receiving")
     * @Security("has_role('ROLE_RECEIVING_ACCOUNT')")
     * @param Request $request
     * @param LearningAgreement $learningAgreement
     * @return Response
     */
    public function approvalReceivingAction(Request $request, LearningAgreement $learningAgreement)
    {
        $duringMobility = $learningAgreement->getDuringMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');
        if (!$authorizationChecker->isGranted('view', $duringMobility)) {
            throw new AccessDeniedHttpException();
        }

        $em = $this->getDoctrine()->getManager();
        $fs = new Filesystem();

        $duringMobility = $learningAgreement->getDuringMobilitySection();

        $studentSignature = $duringMobility->getStudentSignature();
        if ($studentSignature) {
            $signaturePath = "images/signatures/" . $learningAgreement->getId() . '/' . $studentSignature->getFilename() . '.png';
            $studentSignature = $fs->exists($signaturePath) ? $signaturePath : null;
        }

        $sendingSignature = $duringMobility->getSendingSignature();
        if ($sendingSignature) {
            $signaturePath = "images/signatures/" . $learningAgreement->getId() . '/' . $sendingSignature->getFilename() . '.png';
            $sendingSignature = $fs->exists($signaturePath) ? $signaturePath : null;
        }

        $receivingSignature = $duringMobility->getReceivingSignature();
        if ($receivingSignature) {
            $signaturePath = "images/signatures/" . $learningAgreement->getId() . '/' . $receivingSignature->getFilename() . '.png';
            $receivingSignature = $fs->exists($signaturePath) ? $signaturePath : null;
        }

        $form = $this->createFormBuilder()
            ->add('save', SubmitType::class, array(
                'label' => 'Approve',
            ))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($sendingSignature && $receivingSignature) {
                $duringMobility->setIsSignedByReceiving(true);
            }

            $em->flush();

            $clickedButton = $form->getClickedButton()->getName();

            if ($clickedButton != 'save' && $clickedButton != null) {
                $redirectService = $this->get('redirect_service');
                return $redirectService->buttonRedirectAction($learningAgreement, $clickedButton, 'learning_agreement_responsible_persons', null);
            }

            return $this->redirectToRoute('learning_agreements');
        }

        return $this->render('learning-agreements/during-mobility/approval_receiving.html.twig', array(
            'form' => $form->createView(),
            'learningAgreement' => $learningAgreement,
            'studentSignature' => $studentSignature,
            'sendingSignature' => $sendingSignature,
            'receivingSignature' => $receivingSignature
        ));
    }

    /**
     * @Route("/receiving/signature-digital", name="learning_agreement_during_mobility_approval_receiving_signature_digital")
     * @param Request $request
     * @param LearningAgreement $learningAgreement
     * @return Response
     */
    public function receivingSignatureDigitalAction(Request $request, LearningAgreement $learningAgreement)
    {
        $duringMobility = $learningAgreement->getDuringMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');
        if (!$authorizationChecker->isGranted('sign', $duringMobility)) {
            throw new AccessDeniedHttpException();
        }

        $em = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder()
            ->add('signature', HiddenType::class, array(
                'data' => 'signature'
            ))
            ->add('save', SubmitType::class, array(
                'label' => 'Save signature'
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $signatureDataUri = $form->get('signature')->getData();
            if (!($signatureDataUri === 'signature')) { //save only if signature field is not empty
                $encodedImage = explode(",", $signatureDataUri)[1];
                $decodedImage = base64_decode($encodedImage);

                $fs = new Filesystem();
                $dirToSave = "images/signatures/" . $learningAgreement->getId();

                if (!$fs->exists($dirToSave)) {
                    $fs->mkdir($dirToSave);
                }

                $defaultReceivingSignature = $duringMobility->getReceivingSignature();

                $uniqueFileName = $this->generateUniqueFileName();
                file_put_contents($dirToSave . '/' . $uniqueFileName . '.png', $decodedImage);

                $signature = new Signature();
                $signature
                    ->setRole('receiving')
                    ->setAgreementSection($duringMobility)
                    ->setSignedAt(new \DateTime('now', new \DateTimeZone('Europe/Lisbon')))
                    ->setFilename($uniqueFileName)
                    ->setIsDigital(true);

                $em->persist($signature);

                if ($defaultReceivingSignature) {
                    $defaultFile = "images/signatures/" . $learningAgreement->getId() . '/' . $defaultReceivingSignature->getFilename() . '.png';
                    $em->remove($defaultReceivingSignature);
                    $fs->remove($defaultFile);
                }

                $em->flush();
            }

            return $this->redirectToRoute('learning_agreement_during_mobility_approval_receiving', array(
                'learningAgreement' => $learningAgreement->getId()
            ));
        }

        return $this->render('learning-agreements/during-mobility/signature_digital.html.twig', array(
            'form' => $form->createView(),
            'learningAgreement' => $learningAgreement
        ));
    }

    /**
     * @Route("/receiving/signature-upload", name="learning_agreement_during_mobility_approval_receiving_signature_upload")
     * @param Request $request
     * @param LearningAgreement $learningAgreement
     * @return Response
     */
    public function receivingSignatureUploadAction(Request $request, LearningAgreement $learningAgreement)
    {
        $duringMobility = $learningAgreement->getDuringMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');
        if (!$authorizationChecker->isGranted('sign', $duringMobility)) {
            throw new AccessDeniedHttpException();
        }

        $em = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder()
            ->add('signature', FileType::class, array(
            ))
            ->add('save', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $uploadedFile */
            $uploadedFile = $form->get('signature')->getData();

            $fs = new Filesystem();
            $dirToSave = "images/signatures/" . $learningAgreement->getId();

            if (!$fs->exists($dirToSave)) {
                $fs->mkdir($dirToSave);
            }

            $defaultReceivingSignature = $duringMobility->getReceivingSignature();

            $uniqueFileName = $this->generateUniqueFileName();
            $fileName = $uniqueFileName . '.png';
            $uploadedFile->move($dirToSave, $fileName);

            $signature = new Signature();
            $signature
                ->setRole('receiving')
                ->setAgreementSection($duringMobility)
                ->setSignedAt(new \DateTime('now', new \DateTimeZone('Europe/Lisbon')))
                ->setFilename($uniqueFileName)
                ->setIsDigital(false);

            $em->persist($signature);

            if ($defaultReceivingSignature) {
                $defaultFile = "images/signatures/" . $learningAgreement->getId() . '/' . $defaultReceivingSignature->getFilename() . '.png';
                $em->remove($defaultReceivingSignature);
                $fs->remove($defaultFile);
            }

            $em->flush();

            return $this->redirectToRoute('learning_agreement_during_mobility_approval_receiving', array(
                'learningAgreement' => $learningAgreement->getId()
            ));
        }

        return $this->render('learning-agreements/during-mobility/signature_upload.html.twig', array(
            'form' => $form->createView(),
            'learningAgreement' => $learningAgreement
        ));
    }

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        return md5(uniqid());
    }
}