<?php

namespace AppBundle\Controller;

use AppBundle\Entity\LearningAgreement;
use AppBundle\Entity\Signature;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @Route("/learning-agreements/{learningAgreement}/during-mobility/commitment")
 * @Security("has_role('ROLE_STUDENT')")
 */
class DuringMobilityCommitmentController extends Controller
{
    /**
     * @Route("", name="learning_agreement_during_mobility_commitment")
     * @param Request $request
     * @param LearningAgreement $learningAgreement
     * @return Response
     */
    public function commitmentAction(Request $request, LearningAgreement $learningAgreement)
    {
        $duringMobility = $learningAgreement->getDuringMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');
        if (!$authorizationChecker->isGranted('view', $duringMobility)) {
            throw new AccessDeniedHttpException();
        }

        $redirectService = $this->get('redirect_service');
        $hasAllFields = $redirectService->checkRequiredFields($learningAgreement);

        $em = $this->getDoctrine()->getManager();
        $fs = new Filesystem();

        $duringMobility = $learningAgreement->getDuringMobilitySection();

        $studentSignature = $duringMobility->getStudentSignature();
        if ($studentSignature) {
            $signaturePath = "images/signatures/" . $learningAgreement->getId() . '/' . $studentSignature->getFilename() . '.png';
            $studentSignature = $fs->exists($signaturePath) ? $signaturePath : null;
        }

        $sendingSignature = $duringMobility->getSendingSignature();
        if ($sendingSignature) {
            $signaturePath = "images/signatures/" . $learningAgreement->getId() . '/' . $sendingSignature->getFilename() . '.png';
            $sendingSignature = $fs->exists($signaturePath) ? $signaturePath : null;
        }

        $receivingSignature = $duringMobility->getReceivingSignature();
        if ($receivingSignature) {
            $signaturePath = "images/signatures/" . $learningAgreement->getId() . '/' . $receivingSignature->getFilename() . '.png';
            $receivingSignature = $fs->exists($signaturePath) ? $signaturePath : null;
        }

        $form = $this->createFormBuilder()
            ->add('save', SubmitType::class, array(
                'label' => 'Commit',
            ))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($studentSignature) {
                $duringMobility->setIsSignedByStudent(true);
            }

            $em->flush();

            return $this->redirectToRoute('learning_agreements');
        }

        return $this->render('learning-agreements/during-mobility/commitment.html.twig', array(
            'form' => $form->createView(),
            'learningAgreement' => $learningAgreement,
            'studentSignature' => $studentSignature,
            'sendingSignature' => $sendingSignature,
            'receivingSignature' => $receivingSignature,
            'hasAllFields' => $hasAllFields
        ));
    }

    /**
     * @Route("/signature-digital", name="learning_agreement_during_mobility_commitment_signature_digital")
     * @param Request $request
     * @param LearningAgreement $learningAgreement
     * @return Response
     */
    public function signatureDigitalAction(Request $request, LearningAgreement $learningAgreement)
    {
        $duringMobility = $learningAgreement->getDuringMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');
        if (!$authorizationChecker->isGranted('sign', $duringMobility)) {
            throw new AccessDeniedHttpException();
        }

        $em = $this->getDoctrine()->getManager();
        $form = $this->createFormBuilder()
            ->add('signature', HiddenType::class, array(
                'data' => 'signature'
            ))
            ->add('save', SubmitType::class, array(
                'label' => 'Save signature'
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $signatureDataUri = $form->get('signature')->getData();
            if (!($signatureDataUri === 'signature')) { //save only if signature field is not empty
                $encodedImage = explode(",", $signatureDataUri)[1];
                $decodedImage = base64_decode($encodedImage);

                $fs = new Filesystem();
                $dirToSave = "images/signatures/" . $learningAgreement->getId();

                if (!$fs->exists($dirToSave)) {
                    $fs->mkdir($dirToSave);
                }

                $defaultStudentSignature = $learningAgreement->getDuringMobilitySection()->getStudentSignature();

                $uniqueFileName = $this->generateUniqueFileName();
                file_put_contents($dirToSave . '/' . $uniqueFileName . '.png', $decodedImage);

                $signature = new Signature();
                $signature
                    ->setRole('student')
                    ->setAgreementSection($learningAgreement->getDuringMobilitySection())
                    ->setSignedAt(new \DateTime('now', new \DateTimeZone('Europe/Lisbon')))
                    ->setFilename($uniqueFileName)
                    ->setIsDigital(true);

                $em->persist($signature);

                if ($defaultStudentSignature) {
                    $defaultFile = "images/signatures/" . $learningAgreement->getId() . '/' . $defaultStudentSignature->getFilename() . '.png';
                    $em->remove($defaultStudentSignature);
                    $fs->remove($defaultFile);
                }

                $em->flush();
            }

            return $this->redirectToRoute('learning_agreement_during_mobility_commitment', array(
                'learningAgreement' => $learningAgreement->getId()
            ));
        }

        return $this->render('learning-agreements/during-mobility/signature_digital.html.twig', array(
            'form' => $form->createView(),
            'learningAgreement' => $learningAgreement
        ));
    }

    /**
     * @Route("/signature-upload", name="learning_agreement_during_mobility_commitment_signature_upload")
     * @param Request $request
     * @param LearningAgreement $learningAgreement
     * @return Response
     */
    public function signatureUploadAction(Request $request, LearningAgreement $learningAgreement)
    {
        $duringMobility = $learningAgreement->getDuringMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');
        if (!$authorizationChecker->isGranted('sign', $duringMobility)) {
            throw new AccessDeniedHttpException();
        }

        $em = $this->getDoctrine()->getManager();
        $form = $this->createFormBuilder()
            ->add('signature', FileType::class, array(
            ))
            ->add('save', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $uploadedFile */
            $uploadedFile = $form->get('signature')->getData();

            $fs = new Filesystem();
            $dirToSave = "images/signatures/" . $learningAgreement->getId();

            if (!$fs->exists($dirToSave)) {
                $fs->mkdir($dirToSave);
            }

            $defaultStudentSignature = $learningAgreement->getDuringMobilitySection()->getStudentSignature();

            $uniqueFileName = $this->generateUniqueFileName();
            $fileName = $uniqueFileName . '.png';
            $uploadedFile->move($dirToSave, $fileName);

            $signature = new Signature();
            $signature
                ->setRole('student')
                ->setAgreementSection($learningAgreement->getDuringMobilitySection())
                ->setSignedAt(new \DateTime('now', new \DateTimeZone('Europe/Lisbon')))
                ->setFilename($uniqueFileName)
                ->setIsDigital(false);

            $em->persist($signature);

            if ($defaultStudentSignature) {
                $defaultFile = "images/signatures/" . $learningAgreement->getId() . '/' . $defaultStudentSignature->getFilename() . '.png';
                $em->remove($defaultStudentSignature);
                $fs->remove($defaultFile);
            }

            $em->flush();

            return $this->redirectToRoute('learning_agreement_during_mobility_commitment', array(
                'learningAgreement' => $learningAgreement->getId()
            ));
        }

        return $this->render('learning-agreements/during-mobility/signature_upload.html.twig', array(
            'form' => $form->createView(),
            'learningAgreement' => $learningAgreement
        ));
    }

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        return md5(uniqid());
    }
}