<?php

namespace AppBundle\Controller;

use AppBundle\Entity\AgreementSection;
use AppBundle\Entity\EducationalComponent;
use AppBundle\Entity\LearningAgreement;
use AppBundle\Entity\SelectedComponent;
use AppBundle\Entity\User;
use AppBundle\Form\ComponentFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @Route("/learning-agreements/{learningAgreement}/during-mobility")
 */
class DuringMobilityController extends Controller
{
    /**
     * @Route("/create", name="learning_agreement_during_mobility_create")
     * @Security("has_role('ROLE_STUDENT')")
     * @param Request $request
     * @param LearningAgreement $learningAgreement
     * @return Response
     */
    public function newDuringMobilityAction(Request $request, LearningAgreement $learningAgreement)
    {
        // first we have to check if user can create during section for the learning agreement
        $beforeMobility = $learningAgreement->getBeforeMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');
        if (!$authorizationChecker->isGranted('createDuring', $beforeMobility)) {
            throw new AccessDeniedHttpException();
        }

        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $this->getUser();

        if (!$learningAgreement->getDuringMobilitySection()){
            $duringMobility = new AgreementSection();
            $duringMobility
                ->setLearningAgreement($learningAgreement)
                ->setStage('during');

            $em->persist($duringMobility);
            $em->flush();
        }
        else {
            return $this->redirectToRoute('learning_agreement_during_mobility_changes', array(
                'learningAgreement' => $learningAgreement->getId()
            ));
        }

        return $this->redirectToRoute('learning_agreement_during_mobility_changes', array(
            'learningAgreement' => $learningAgreement->getId()
        ));
    }

    /**
     * @Route("/delete", name="learning_agreement_during_mobility_delete")
     * @Security("has_role('ROLE_STUDENT')")
     * @param Request $request
     * @param LearningAgreement $learningAgreement
     * @return Response
     */
    public function deleteAction(Request $request, LearningAgreement $learningAgreement)
    {
        $duringMobility = $learningAgreement->getDuringMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');
        if (!$authorizationChecker->isGranted('remove', $duringMobility)) {
            throw new AccessDeniedHttpException();
        }

        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $this->getUser();

        if ($learningAgreement->getStudent() === $user) {
            $em->remove($learningAgreement->getDuringMobilitySection());
            $em->flush();
        }
        else {
            throw new AccessDeniedException('');
        }

        return $this->redirectToRoute('learning_agreements');
    }

        /**
     * @Route("/changes", name="learning_agreement_during_mobility_changes")
     * @param Request $request
     * @param LearningAgreement $learningAgreement
     * @return Response
     */
    public function duringMobilityAction(Request $request, LearningAgreement $learningAgreement)
    {
        $duringMobility = $learningAgreement->getDuringMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');
        if (!$authorizationChecker->isGranted('view', $duringMobility)) {
            throw new AccessDeniedHttpException();
        }

        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $this->getUser();

        $beforeMobilityReceivingComponents = $learningAgreement->getReceivingInstitution()->getBeforeMobilityComponents();
        $beforeMobilitySendingComponents = $learningAgreement->getSendingInstitution()->getBeforeMobilityComponents();

        $duringMobilityReceivingComponents = $learningAgreement->getReceivingInstitution()->getDuringMobilityComponents();
        $duringMobilitySendingComponents = $learningAgreement->getSendingInstitution()->getDuringMobilityComponents();

        $receivingComponents = array();
        $receivingComponents['before'] = $beforeMobilityReceivingComponents;
        $receivingComponents['during'] = $duringMobilityReceivingComponents;

        $sendingComponents = array();
        $sendingComponents['before'] = $beforeMobilitySendingComponents;
        $sendingComponents['during'] = $duringMobilitySendingComponents;

        return $this->render(':learning-agreements/during-mobility:changes_in_mobility.html.twig', array(
            'learningAgreement' => $learningAgreement,
            'receivingComponents' => $receivingComponents,
            'sendingComponents' => $sendingComponents
        ));
    }

    /**
     * @Route("/component/{role}/new", name="learning_agreement_during_mobility_component_new")
     * @Security("has_role('ROLE_STUDENT') or has_role('ROLE_ADMIN')")
     * @param LearningAgreement $learningAgreement
     * @return Response
     */
    public function newComponentAction(LearningAgreement $learningAgreement, $role)
    {
        $duringMobility = $learningAgreement->getDuringMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');
        if (!$authorizationChecker->isGranted('edit', $duringMobility)) {
            throw new AccessDeniedHttpException();
        }

        $em = $this->getDoctrine()->getManager();

        $duringMobility = $learningAgreement->getDuringMobilitySection();

        $selectedComponent = new SelectedComponent();

        if ($role === 'sending') { // create new component for Table A (sending)
            $sendingInstitution = $learningAgreement->getSendingInstitution();
            $selectedComponent->setInvolvedInstitution($sendingInstitution);
        }
        else { // create new component for Table B (receiving)
            $receivingInstitution = $learningAgreement->getReceivingInstitution();
            $selectedComponent->setInvolvedInstitution($receivingInstitution);
        }

        $selectedComponent
            ->setIsAdded(true)
            ->setAgreementSection($duringMobility);

        $em->persist($selectedComponent);
        $em->flush();

        return $this->redirectToRoute('learning_agreement_during_mobility_changes_component_detail', array(
            'learningAgreement' => $learningAgreement->getId(),
            'selectedComponent' => $selectedComponent->getId()
        ));
    }

    /**
     * @Route("/component/{selectedComponent}", name="learning_agreement_during_mobility_changes_component_detail")
     * @Security("has_role('ROLE_STUDENT') or has_role('ROLE_ADMIN')")
     * @param Request $request
     * @param LearningAgreement $learningAgreement
     * @param SelectedComponent $selectedComponent
     * @return Response
     */
    public function componentDetailAction(Request $request, LearningAgreement $learningAgreement, SelectedComponent $selectedComponent)
    {
        $duringMobility = $learningAgreement->getDuringMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');
        if (!$authorizationChecker->isGranted('edit', $duringMobility)) {
            throw new AccessDeniedHttpException();
        }

        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $this->getUser();

        $educationalComponent = $selectedComponent->getEducationalComponent() ? : new EducationalComponent();

        if ($selectedComponent->isBefore()){
            $disabled = true; // used in creating form below to disable inputs in component detail
            // to prevent editing component from before mobility stage
            $choices = array(
                'Previously selected educational component is not available at the Receiving Institution' => '1',
                'Component is in a different language than previously specified in the course catalogue' => '2',
                'Timetable conflict' => '3',
                'Other' => '4'
            );
        }
        else {
            $disabled = false;
            $choices = array(
                'Substituting a deleted component' => '5',
                'Extending the mobility period' => '6',
                'Other' => '7'
            );
        }
        $form = $this->createForm(ComponentFormType::class, $educationalComponent, array('disable' => $disabled));
        if ($selectedComponent->getInvolvedInstitution()->getRoleInAgreement() === 'receiving'){
            $form->add('reason', ChoiceType::class, array(
                'disabled' => false,
                'mapped' => false,
                'choices' => $choices,
                'data' => $selectedComponent->getReason(),
                'placeholder' => '---'
            ));
        }

        if (!$user->hasRole('ROLE_RECEIVING_ACCOUNT')) {
            $form
                ->add('save', SubmitType::class);
        }

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($educationalComponent);
            $selectedComponent->setEducationalComponent($educationalComponent);

            if (!$selectedComponent->isSending()){
                $reason = $form->get('reason')->getData();
                $selectedComponent->setReason($reason);
                if ($selectedComponent->isBefore()){
                    $selectedComponent->setIsDeleted(true);
                }
            }

            $em->flush();

            return $this->redirectToRoute('learning_agreement_during_mobility_changes', array(
                'learningAgreement' => $learningAgreement->getId(),
            ));
        }

        return $this->render('learning-agreements/during-mobility/component_detail.html.twig', array(
            'form' => $form->createView(),
            'learningAgreement' => $learningAgreement,
            'roleInAgreement' => $selectedComponent->getInvolvedInstitution()->getRoleInAgreement()
        ));
    }

    /**
     * @Route("/component/{selectedComponent}/delete", name="learning_agreement_during_mobility_component_delete")
     * @Security("has_role('ROLE_STUDENT') or has_role('ROLE_ADMIN')")
     * @param Request $request
     * @param LearningAgreement $learningAgreement
     * @param SelectedComponent $selectedComponent
     * @return Response
     */
    public function componentDeleteAction(Request $request, LearningAgreement $learningAgreement, SelectedComponent $selectedComponent)
    {
        $duringMobility = $learningAgreement->getDuringMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');
        if (!$authorizationChecker->isGranted('edit', $duringMobility)) {
            throw new AccessDeniedHttpException();
        }

        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $this->getUser();

        if ($user->hasRole('ROLE_ADMIN') or ($learningAgreement->getStudent() === $user)) {
            if (!$selectedComponent->isBefore()){
                $em->remove($selectedComponent);
                $em->flush();
            }
        }
        else {
            throw new AccessDeniedHttpException();
        }

        return $this->redirectToRoute('learning_agreement_during_mobility_changes', array(
            'learningAgreement' => $learningAgreement->getId()
        ));
    }

    /**
     * @Route("/component/{selectedComponent}/toggle-deleted", name="learning_agreement_during_mobility_component_toggle_deleted")
     * @Security("has_role('ROLE_STUDENT') or has_role('ROLE_ADMIN')")
     * @param Request $request
     * @param LearningAgreement $learningAgreement
     * @param SelectedComponent $selectedComponent
     * @return Response
     */
    public function componentSetAsDeletedAction(Request $request, LearningAgreement $learningAgreement, SelectedComponent $selectedComponent)
    {
        $duringMobility = $learningAgreement->getDuringMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');
        if (!$authorizationChecker->isGranted('edit', $duringMobility)) {
            throw new AccessDeniedHttpException();
        }

        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $this->getUser();

        if ($selectedComponent->isBefore()){
            if ($selectedComponent->getIsDeleted()) {
                $selectedComponent->setIsDeleted(false);
                $selectedComponent->setReason(null);
            } elseif ($selectedComponent->isSending() && !$selectedComponent->getIsDeleted()) {
                $selectedComponent->setIsDeleted(true);
            }
            $em->flush();
        }

        return $this->redirectToRoute('learning_agreement_during_mobility_changes', array(
            'learningAgreement' => $learningAgreement->getId()
        ));
    }
}