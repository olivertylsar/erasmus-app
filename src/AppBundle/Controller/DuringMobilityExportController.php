<?php

namespace AppBundle\Controller;

use AppBundle\Entity\LearningAgreement;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @Route("/learning-agreements/{learningAgreement}/during-mobility/export")
 */
class DuringMobilityExportController extends Controller
{
    /**
     * Generate and save a PDF
     * @Route("/pdf", name="learning_agreement_during_mobility_export_pdf")
     * @param LearningAgreement $learningAgreement
     * @return Response
     */
    public function pdfAction(LearningAgreement $learningAgreement, Request $request)
    {
        $beforeMobility = $learningAgreement->getBeforeMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');

        if (!$authorizationChecker->isGranted('export', $beforeMobility)) {
            throw new AccessDeniedHttpException();
        }

        $beforeMobilityReceivingComponents = $learningAgreement->getReceivingInstitution()->getBeforeMobilityComponents();
        $beforeMobilitySendingComponents = $learningAgreement->getSendingInstitution()->getBeforeMobilityComponents();

        $duringMobilityReceivingComponents = $learningAgreement->getReceivingInstitution()->getDuringMobilityComponents();
        $duringMobilitySendingComponents = $learningAgreement->getSendingInstitution()->getDuringMobilityComponents();

        $receivingComponents = array();
        $receivingComponents['before'] = $beforeMobilityReceivingComponents;
        $receivingComponents['during'] = $duringMobilityReceivingComponents;

        $sendingComponents = array();
        $sendingComponents['before'] = $beforeMobilitySendingComponents;
        $sendingComponents['during'] = $duringMobilitySendingComponents;

        $html = $this->renderView('export/learning_agreement_during_mobility_export.html.twig', array(
            'learningAgreement' => $learningAgreement,
            'receivingComponents' => $receivingComponents,
            'sendingComponents' => $sendingComponents,
        ));

        $header = $this->renderView(':export:header.html.twig');
        $footer = $this->renderView(':export:footer.html.twig');

        $output = 'files/pdf/la-'. $learningAgreement->getId() .'.pdf';

        // Generate PDF file
        $this->get('knp_snappy.pdf')->generateFromHtml($html, $output, array(
            'header-html' => $header,
            'footer-html' => $footer,
            'encoding' => 'utf-8',
            'images' => true
        ), true);

        // Redirect to that PDF file
        $kernel = $this->get('kernel')->getRootDir();
        $path = $kernel. '/../web/files/pdf/'.'la-'.$learningAgreement->getId() .'.pdf';

        $response = new BinaryFileResponse($path);
        $response->headers->set('Content-Type', 'application/pdf');
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_INLINE, 'UPT_Learning-Agreement_' . $learningAgreement->getStudent()->getLastName() . '-' . $learningAgreement->getId() .'.pdf');
        //use ResponseHeaderBag::DISPOSITION_ATTACHMENT to save as an attachement

        return $response;
    }

    /**
     * Generate and save a PDF
     * @Route("/html", name="learning_agreement_export_html")
     * @param LearningAgreement $learningAgreement
     * @return Response
     */
    public function htmlAction(LearningAgreement $learningAgreement, Request $request)
    {
        $beforeMobility = $learningAgreement->getBeforeMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');

        if (!$authorizationChecker->isGranted('export', $beforeMobility)) {
            throw new AccessDeniedHttpException();
        }

        $receivingComponents = $learningAgreement->getReceivingInstitution()->getSelectedComponents();
        $sendingComponents = $learningAgreement->getSendingInstitution()->getSelectedComponents();

        $receivingCredits = $this->getTotalCredits($receivingComponents);
        $sendingCredits = $this->getTotalCredits($sendingComponents);

        $credits = array();
        $credits['receiving'] = $receivingCredits;
        $credits['sending'] = $sendingCredits;

        return $this->render('export/learning_agreement_export.html.twig', array(
            'learningAgreement' => $learningAgreement,
            'receivingComponents' => $receivingComponents,
            'sendingComponents' => $sendingComponents,
            'credits' => $credits
        ));
    }

    /**
     * @param Collection $components
     * @return int
     */
    private function getTotalCredits(Collection $components)
    {
        $credits = 0;
        foreach ($components as $component) {
            /** EducationalComponent $component */
            if ($component->getEducationalComponent()) {
                $credits += $component->getEducationalComponent()->getEctsCredits();
            }
        }
        return $credits;
    }
}
