<?php

namespace AppBundle\Controller;

use AppBundle\Entity\InstitutionPerson;
use AppBundle\Entity\InvolvedInstitution;
use AppBundle\Entity\InvolvedPerson;
use AppBundle\Entity\LearningAgreement;
use AppBundle\Entity\User;
use AppBundle\Form\InvolvedInstitutionFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @Route("/learning-agreements")
 */
class InstitutionController extends Controller
{
    /**
     * @Route("/{learningAgreement}/sending-institution", name="learning_agreement_sending_institution")
     * @param Request $request
     * @param LearningAgreement $learningAgreement
     * @return Response
     */
    public function sendingInstitutionAction(Request $request, LearningAgreement $learningAgreement)
    {
        $beforeMobility = $learningAgreement->getBeforeMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');

        if (!$authorizationChecker->isGranted('view', $beforeMobility)) {
            throw new AccessDeniedHttpException();
        }

        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $this->getUser();

        /** @var InvolvedInstitution $sendingInstitution */
        $sendingInstitution = $learningAgreement->getSendingInstitution();

        $disabled = false;
        if (!$authorizationChecker->isGranted('edit', $beforeMobility)){
            $disabled = true;
        }

        $contactPerson = $sendingInstitution->getContactPerson();
        if (!$contactPerson) {
            $contactPerson = new InvolvedPerson('contact');
            $contactPerson->setInvolvedInstitution($sendingInstitution);
            $em->persist($contactPerson);
            $em->flush();

            return $this->redirectToRoute('learning_agreement_sending_institution', array(
                'learningAgreement' => $learningAgreement->getId()
            ));
        }

        $form = $this->createForm(InvolvedInstitutionFormType::class, $sendingInstitution, array('role' => 'sending', 'involved_institution' => $sendingInstitution, 'disabled' => $disabled));
        if (!$user->hasRole('ROLE_RECEIVING_ACCOUNT') && $authorizationChecker->isGranted('edit', $beforeMobility)) {
            $form
                ->add('previous', SubmitType::class)
                ->add('save', SubmitType::class)
                ->add('next', SubmitType::class);
        }
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $contactPerson->setInstitutionPerson($form->get('contactPerson')->getData());

            $em->persist($contactPerson);

            $em->persist($sendingInstitution);
            $em->flush();

            $clickedButton = $form->getClickedButton()->getName();

            if ($clickedButton != 'save' && $clickedButton != null) {
                $redirectService = $this->get('redirect_service');
                return $redirectService->buttonRedirectAction($learningAgreement, $clickedButton, 'learning_agreement_student_info', 'learning_agreement_receiving_institution');
            }

            return $this->redirectToRoute('learning_agreement_sending_institution', array(
                'learningAgreement' => $learningAgreement->getId()
            ));
        }

        return $this->render('learning-agreements/sending_institution.html.twig', array(
            'form' => $form->createView(),
            'learningAgreement' => $learningAgreement
        ));
    }

    /**
     * @Route("/{learningAgreement}/receiving-institution", name="learning_agreement_receiving_institution")
     * @param Request $request
     * @param LearningAgreement $learningAgreement
     * @return Response
     */
    public function receivingInstitutionAction(Request $request, LearningAgreement $learningAgreement)
    {
        $beforeMobility = $learningAgreement->getBeforeMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');

        if (!$authorizationChecker->isGranted('view', $beforeMobility)) {
            throw new AccessDeniedHttpException();
        }

        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $this->getUser();

        /** @var InvolvedInstitution $receivingInstitution */
        $receivingInstitution = $learningAgreement->getReceivingInstitution();

        $disabled = false;
        if (!$authorizationChecker->isGranted('edit', $beforeMobility)){
            $disabled = true;
        }

        if (!$receivingInstitution) {
            $receivingInstitution = new InvolvedInstitution($learningAgreement, 'receiving');
        }

        $involvedPerson = $receivingInstitution->getContactPerson();
        if ($involvedPerson) {
            $institutionPerson = $receivingInstitution->getContactPerson()->getInstitutionPerson();
        }
        else {
            $institutionPerson = new InstitutionPerson();
            $involvedPerson = new InvolvedPerson('contact');
            $involvedPerson->setInvolvedInstitution($receivingInstitution);
            $involvedPerson->setInstitutionPerson($institutionPerson);
        }

        $em->persist($institutionPerson);
        $em->persist($involvedPerson);
        $em->flush();

        $beforeMobility = $learningAgreement->getBeforeMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');

        $form = $this->createForm(InvolvedInstitutionFormType::class, $receivingInstitution, array('role' => 'receiving', 'involved_institution' => $receivingInstitution, 'disabled' => $disabled));
        if (!$user->hasRole('ROLE_RECEIVING_ACCOUNT') && $authorizationChecker->isGranted('edit', $beforeMobility)) {
            $form
                ->add('previous', SubmitType::class)
                ->add('save', SubmitType::class)
                ->add('next', SubmitType::class);
        }
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $institution = $form->get('institution')->getData();
            $receivingInstitution->setInstitution($institution);

            $em->persist($receivingInstitution);

            $institutionPerson
                ->setName($form->get('contactPersonName')->getData())
                ->setEmail($form->get('contactPersonEmail')->getData())
                ->setPhoneNumber($form->get('contactPersonPhone')->getData())
                ->setInstitution($receivingInstitution->getInstitution());

            $involvedPerson
                ->setInvolvedInstitution($receivingInstitution);

            $em->persist($institutionPerson);
            $em->persist($involvedPerson);
            $em->flush();

            $clickedButton = $form->getClickedButton()->getName();

            if ($clickedButton != 'save' && $clickedButton != null) {
                $redirectService = $this->get('redirect_service');
                return $redirectService->buttonRedirectAction($learningAgreement, $clickedButton, 'learning_agreement_sending_institution', 'learning_agreement_mobility_programme');
            }

            return $this->redirectToRoute('learning_agreement_receiving_institution', array(
                'learningAgreement' => $learningAgreement->getId()
            ));
        }

        return $this->render('learning-agreements/receiving_institution.html.twig', array(
            'form' => $form->createView(),
            'learningAgreement' => $learningAgreement
        ));
    }
}