<?php

namespace AppBundle\Controller;

use AppBundle\Entity\AgreementSection;
use AppBundle\Entity\InvolvedInstitution;
use AppBundle\Entity\LearningAgreement;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @Route("/learning-agreements")
 */
class LearningAgreementsController extends Controller
{
    /**
     * @Route("", name="learning_agreements")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $this->getUser();

        $accountType = $user->getAccountType()->getAccountTypeName();
        switch ($accountType){
            case 'Teacher':
            case 'Staff': $learningAgreements = $em->getRepository('AppBundle:LearningAgreement')->findAll();
                break;
            case 'Student': $learningAgreements = $user->getLearningAgreements();
                break;
            case 'Receiving University Account':
                $learningAgreements = $em->getRepository('AppBundle:LearningAgreement')->findAll();
                $learningAgreementsForRP = array();
                foreach ($learningAgreements as $learningAgreement) {
                    $receivingResponsiblePerson = $learningAgreement->getReceivingInstitution()->getResponsiblePerson();
                    if ($learningAgreement->getBeforeMobilitySection()->getIsSignedBySending() && $receivingResponsiblePerson && $learningAgreement->isResponsiblePersonLoggedIn($user)) {
                        $learningAgreementsForRP[] = $learningAgreement;
                    }
                }
                $learningAgreements = $learningAgreementsForRP;
                break;
            default: $learningAgreements = null;
        }

        $duringAgreementSections = $em->getRepository('AppBundle:AgreementSection')->findBy(array(
            'stage' => 'during'
        ));

        $learningAgreementsWithDuring = array();
        foreach ($duringAgreementSections as $duringAgreementSection) {
            $learningAgreement = $duringAgreementSection->getLearningAgreement();
            $learningAgreementsWithDuring[] = $learningAgreement;
        }

        return $this->render('learning-agreements/index.html.twig', array(
            'learningAgreements' => $learningAgreements,
            'learningAgreementsWithDuring' => $learningAgreementsWithDuring
        ));
    }

    /**
     * @Route("/new", name="learning_agreements_new")
     * @Security("has_role('ROLE_STUDENT')")
     * @param Request $request
     * @return Response
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $this->getUser();
        $learningAgreement = new LearningAgreement();
        $learningAgreement->setStudent($user);

        $beforeMobilitySection = new AgreementSection();
        $beforeMobilitySection
            ->setLearningAgreement($learningAgreement)
            ->setStage('before');

        $upt = $em->getRepository('AppBundle:Institution')->find(1);

        $sendingInstitution = new InvolvedInstitution($learningAgreement, 'sending');
        $sendingInstitution->setInstitution($upt);
        $receivingInstitution = new InvolvedInstitution($learningAgreement, 'receiving');

        $em->persist($learningAgreement);
        $em->persist($beforeMobilitySection);
        $em->persist($sendingInstitution);
        $em->persist($receivingInstitution);
        $em->flush();

        return $this->redirectToRoute('learning_agreement_student_info', array(
            'learningAgreement' => $learningAgreement->getId()
        ));
    }

    /**
     * @Route("/{learningAgreement}/delete", name="learning_agreement_delete")
     * @Security("has_role('ROLE_STUDENT') or has_role('ROLE_ADMIN')")
     * @param Request $request
     * @param LearningAgreement $learningAgreement
     * @return Response
     */
    public function deleteAction(Request $request, LearningAgreement $learningAgreement)
    {
        $beforeMobility = $learningAgreement->getBeforeMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');
        if (!$authorizationChecker->isGranted('remove', $beforeMobility)) {
            throw new AccessDeniedHttpException();
        }

        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $this->getUser();

        if ($learningAgreement->getStudent() === $user) {
            $em->remove($learningAgreement);
            $em->flush();
        }
        else {
            throw new AccessDeniedException('');
        }

        return $this->redirectToRoute('learning_agreements');
    }
}
