<?php

namespace AppBundle\Controller;

use AppBundle\Entity\EducationalComponent;
use AppBundle\Entity\LearningAgreement;
use AppBundle\Entity\SelectedComponent;
use AppBundle\Entity\User;
use AppBundle\Form\ComponentFormType;
use AppBundle\Form\MobilityProgrammeFormType;
use Doctrine\Common\Collections\Collection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @Route("/learning-agreements/{learningAgreement}/mobility-programme")
 */
class MobilityProgrammeController extends Controller
{
    /**
     * @Route("", name="learning_agreement_mobility_programme")
     * @param Request $request
     * @param LearningAgreement $learningAgreement
     * @return Response
     */
    public function mobilityProgrammeAction(Request $request, LearningAgreement $learningAgreement)
    {
        $beforeMobility = $learningAgreement->getBeforeMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');

        if (!$authorizationChecker->isGranted('view', $beforeMobility)) {
            throw new AccessDeniedHttpException();
        }

        $disabled = false;
        if (!$authorizationChecker->isGranted('edit', $beforeMobility)){
            $disabled = true;
        }

        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $this->getUser();

        $receivingInstitution = $learningAgreement->getReceivingInstitution();
        $sendingInstitution = $learningAgreement->getSendingInstitution();

        $receivingComponents = $learningAgreement->getReceivingInstitution()->getBeforeMobilityComponents();
        $sendingComponents = $learningAgreement->getSendingInstitution()->getBeforeMobilityComponents();

        $receivingCredits = $this->getTotalCredits($receivingComponents);
        $sendingCredits = $this->getTotalCredits($sendingComponents);

        $credits = array();
        $credits['receiving'] = $receivingCredits;
        $credits['sending'] = $sendingCredits;

        $involvedInstitutions = array(
            'sending' => $learningAgreement->getSendingInstitution(),
            'receiving' => $learningAgreement->getReceivingInstitution()
        );

        $form = $this->createForm(MobilityProgrammeFormType::class, $learningAgreement, array('involved_institutions' => $involvedInstitutions, 'disabled' => $disabled));
        if (!$user->hasRole('ROLE_RECEIVING_ACCOUNT') && $authorizationChecker->isGranted('edit', $beforeMobility)) {
            $form
                ->add('previous', SubmitType::class, array(
                    'validation_groups' => false
                ))
                ->add('save', SubmitType::class)
                ->add('next', SubmitType::class, array());
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($learningAgreement);

            $receivingCourseCatalogueLink = $form->get('receivingCourseCatalogueLink')->getData();
            $sendingCourseCatalogueLink = $form->get('sendingCourseCatalogueLink')->getData();

            $receivingInstitution->setCourseCatalogueLink($receivingCourseCatalogueLink);
            $sendingInstitution->setCourseCatalogueLink($sendingCourseCatalogueLink);

            $em->flush();

            $clickedButton = $form->getClickedButton()->getName();

            if ($clickedButton != 'save' && $clickedButton != null) {
                $redirectService = $this->get('redirect_service');
                return $redirectService->buttonRedirectAction($learningAgreement, $clickedButton, 'learning_agreement_receiving_institution', 'learning_agreement_responsible_persons');
            }

            return $this->redirectToRoute('learning_agreement_mobility_programme', array(
                'learningAgreement' => $learningAgreement->getId()
            ));
        }

        return $this->render('learning-agreements/mobility_programme.html.twig', array(
            'form' => $form->createView(),
            'learningAgreement' => $learningAgreement,
            'receivingComponents' => $receivingComponents,
            'sendingComponents' => $sendingComponents,
            'credits' => $credits
        ));
    }

    /**
     * @Route("/component/{role}/new", name="learning_agreement_mobility_programme_component_new")
     * @Security("has_role('ROLE_STUDENT') or has_role('ROLE_ADMIN')")
     * @param LearningAgreement $learningAgreement
     * @return Response
     */
    public function newComponentAction(LearningAgreement $learningAgreement, $role)
    {
        $beforeMobility = $learningAgreement->getBeforeMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');

        if (!$authorizationChecker->isGranted('edit', $beforeMobility)) {
            throw new AccessDeniedHttpException();
        }

        $em = $this->getDoctrine()->getManager();

        $selectedComponent = new SelectedComponent();

        if ($role === 'sending') { // create new component for Table A (sending)
            $sendingInstitution = $learningAgreement->getSendingInstitution();
            $selectedComponent->setInvolvedInstitution($sendingInstitution);
        }
        else { // create new component for Table B (receiving)
            $receivingInstitution = $learningAgreement->getReceivingInstitution();
            $selectedComponent->setInvolvedInstitution($receivingInstitution);
        }

        $beforeMobility = $learningAgreement->getBeforeMobilitySection();
        $selectedComponent->setAgreementSection($beforeMobility);

        $em->persist($selectedComponent);
        $em->flush();

        return $this->redirectToRoute('learning_agreement_mobility_programme_component_detail', array(
            'learningAgreement' => $learningAgreement->getId(),
            'selectedComponent' => $selectedComponent->getId()
        ));
    }

    /**
     * @Route("/component/{selectedComponent}", name="learning_agreement_mobility_programme_component_detail")
     * @Security("has_role('ROLE_STUDENT') or has_role('ROLE_ADMIN')")
     * @param Request $request
     * @param LearningAgreement $learningAgreement
     * @param SelectedComponent $selectedComponent
     * @return Response
     */
    public function componentDetailAction(Request $request, LearningAgreement $learningAgreement, SelectedComponent $selectedComponent)
    {
        $beforeMobility = $learningAgreement->getBeforeMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');
        if (!$authorizationChecker->isGranted('edit', $beforeMobility)) {
            throw new AccessDeniedHttpException();
        }

        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $this->getUser();

        $educationalComponent = $selectedComponent->getEducationalComponent() ? : new EducationalComponent();

        $form = $this->createForm(ComponentFormType::class, $educationalComponent);
        if (!$user->hasRole('ROLE_RECEIVING_ACCOUNT')) {
            $form
                ->add('save', SubmitType::class);
        }

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($educationalComponent);
            $selectedComponent->setEducationalComponent($educationalComponent);
            $em->flush();

            return $this->redirectToRoute('learning_agreement_mobility_programme', array(
                'learningAgreement' => $learningAgreement->getId(),
            ));
        }

        return $this->render('learning-agreements/component_detail.html.twig', array(
            'form' => $form->createView(),
            'learningAgreement' => $learningAgreement
        ));
    }

    /**
     * @Route("/component/{selectedComponent}/delete", name="learning_agreement_mobility_programme_component_delete")
     * @Security("has_role('ROLE_STUDENT') or has_role('ROLE_ADMIN')")
     * @param Request $request
     * @param LearningAgreement $learningAgreement
     * @param SelectedComponent $selectedComponent
     * @return Response
     */
    public function componentDeleteAction(Request $request, LearningAgreement $learningAgreement, SelectedComponent $selectedComponent)
    {
        $beforeMobility = $learningAgreement->getBeforeMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');
        if (!$authorizationChecker->isGranted('edit', $beforeMobility)) {
            throw new AccessDeniedHttpException();
        }

        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $this->getUser();

        $em->remove($selectedComponent);
        $em->flush();

        return $this->redirectToRoute('learning_agreement_mobility_programme', array(
            'learningAgreement' => $learningAgreement->getId()
        ));
    }

    /**
     * @param Collection $components
     * @return int
     */
    private function getTotalCredits(Collection $components)
    {
        $credits = 0;
        foreach ($components as $component) {
            /** EducationalComponent $component */
            if ($component->getEducationalComponent()) {
                $credits += $component->getEducationalComponent()->getEctsCredits();
            }
        }
        return $credits;
    }
}