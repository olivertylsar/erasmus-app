<?php

namespace AppBundle\Controller;

use AppBundle\Entity\LearningAgreement;
use AppBundle\Entity\SelectedComponent;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RedirectController extends Controller
{
    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param LearningAgreement $learningAgreement
     * @param $clickedButton
     * @param $prev
     * @param $next
     * @return null|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function buttonRedirectAction(LearningAgreement $learningAgreement, $clickedButton, $prev, $next)
    {
        /** @var User $user */
        $user = $this->getUser();
        if ($next == 'learning_agreement_commitment') {
            if ($user->hasRole('ROLE_ADMIN')) {
                $next = 'learning_agreement_approval';
            }
        }
        if ($clickedButton == 'previous') {
            return $this->redirectToRoute($prev, array(
                'learningAgreement' => $learningAgreement->getId()
            ));
        }
        if ($clickedButton == 'next') {
            return $this->redirectToRoute($next, array(
                'learningAgreement' => $learningAgreement->getId()
            ));
        }
        return null;
    }

    public function checkRequiredFields(LearningAgreement $learningAgreement)
    {
        $allFields = [];

        // student info
        /** @var User $student */
        $student = $learningAgreement->getStudent();
        $allFields[] = $student->getFirstName();
        $allFields[] = $student->getLastName();
        $allFields[] = $student->getDateOfBirth();
        $allFields[] = $student->getCountry();
        $allFields[] = $student->getSex();
        $allFields[] = $student->getUniversityIdNumber();
        $allFields[] = $student->getPhoneNumber();
        $allFields[] = $learningAgreement->getStudyCycle();
        $allFields[] = $learningAgreement->getAcademicYear();
        $allFields[] = $learningAgreement->getFieldOfEducation();

        // sending institution
        $sendingInstitution = $learningAgreement->getSendingInstitution();
        $allFields[] = $sendingInstitution->getFaculty();
        $allFields[] = $sendingInstitution->getDepartment();
        $allFields[] = $sendingInstitution->getContactPerson();

        // receiving institution
        $receivingInstitution = $learningAgreement->getReceivingInstitution();
        $allFields[] = $receivingInstitution->getInstitution();
        $allFields[] = $receivingInstitution->getFaculty();
        $allFields[] = $receivingInstitution->getDepartment();
        $allFields[] = $receivingInstitution->getContactPerson();

        // mobility programme
        $allFields[] = $sendingInstitution->getCourseCatalogueLink();
        $allFields[] = $receivingInstitution->getCourseCatalogueLink();
        $allFields[] = $learningAgreement->getStartDate();
        $allFields[] = $learningAgreement->getEndDate();
        $allFields[] = $learningAgreement->getLanguage();
        $allFields[] = $learningAgreement->getLanguageCompetence();
        foreach ($learningAgreement->getInvolvedInstitutions() as $involvedInstitution){
            foreach ($involvedInstitution->getSelectedComponents() as $component){
                /** @var SelectedComponent $component */
                $allFields[] = $component->getEducationalComponent();
            }
        }

        // responsible persons
        $sendingResponsiblePerson = $sendingInstitution->getResponsiblePerson();
        $receivingResponsiblePerson = $receivingInstitution->getResponsiblePerson();

        $allFields[] = $sendingResponsiblePerson ? $sendingResponsiblePerson->getInstitutionPerson() : null;
        $allFields[] = $receivingResponsiblePerson ? $receivingResponsiblePerson->getInstitutionPerson() : null;

        if (in_array(null, $allFields, true )) {
            return array(
                'ok' => false,
                'message' => 'Please fill all the required fields in previous steps to be able to sign and commit this learning agreement or its changes.'
            );
        }
        else {
            return array(
                'ok' => true
            );
        }
    }
}