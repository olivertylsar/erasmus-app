<?php

namespace AppBundle\Controller;

use AppBundle\Entity\InstitutionPerson;
use AppBundle\Entity\InvolvedPerson;
use AppBundle\Entity\LearningAgreement;
use AppBundle\Entity\User;
use AppBundle\Form\ResponsiblePersonFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @Route("/learning-agreements")
 */
class ResponsiblePersonsController extends Controller
{
    /**
     * @Route("/{learningAgreement}/responsible-persons", name="learning_agreement_responsible_persons")
     * @param Request $request
     * @param LearningAgreement $learningAgreement
     * @return Response
     */
    public function responsiblePersonsAction(Request $request, LearningAgreement $learningAgreement)
    {
        $beforeMobility = $learningAgreement->getBeforeMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');

        if (!$authorizationChecker->isGranted('view', $beforeMobility)) {
            throw new AccessDeniedHttpException();
        }

        $disabled = false;
        if (!$authorizationChecker->isGranted('edit', $beforeMobility)){
            $disabled = true;
        }

        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $this->getUser();

        $sendingInstitution = $learningAgreement->getSendingInstitution();
        $receivingInstitution = $learningAgreement->getReceivingInstitution();

        $sendingResponsiblePerson = $sendingInstitution ? $sendingInstitution->getResponsiblePerson() : null;
        $receivingResponsiblePerson = $receivingInstitution ? $receivingInstitution->getResponsiblePerson() : null;

        if (!$sendingResponsiblePerson) {
            $sendingResponsiblePerson = new InvolvedPerson('responsible');
            $sendingResponsiblePerson
                ->setInvolvedInstitution($sendingInstitution);

            $em->persist($sendingResponsiblePerson);
        }

        $sendingInstitutionPerson = $sendingResponsiblePerson ? $sendingResponsiblePerson->getInstitutionPerson() : null;

        if (!$receivingResponsiblePerson) {
            $receivingInstitutionPerson = new InstitutionPerson();

            $receivingResponsiblePerson = new InvolvedPerson('responsible');
            $receivingResponsiblePerson
                ->setInstitutionPerson($receivingInstitutionPerson)
                ->setInvolvedInstitution($receivingInstitution);

            $em->persist($receivingInstitutionPerson);
            $em->persist($receivingResponsiblePerson);
        }
        else {
            $receivingInstitutionPerson = $receivingResponsiblePerson->getInstitutionPerson();
        }

        $em->flush();

        if (!$user->hasRole('ROLE_RECEIVING_ACCOUNT') && $authorizationChecker->isGranted('edit', $beforeMobility)) {
            $form = $this->createFormBuilder()
                ->add('sending', ResponsiblePersonFormType::class, array('role' => 'sending', 'institutionPerson' => $sendingInstitutionPerson))
                ->add('receiving', ResponsiblePersonFormType::class, array('role' => 'receiving', 'institutionPerson' => $receivingInstitutionPerson))
                ->add('previous', SubmitType::class, array(
                    'validation_groups' => false
                ))
                ->add('save', SubmitType::class)
                ->add('next', SubmitType::class)
                ->getForm();
        }
        else{
            $form = $this->createFormBuilder()
                ->add('sending', ResponsiblePersonFormType::class, array('role' => 'sending', 'institutionPerson' => $sendingInstitutionPerson, 'disabled' => $disabled))
                ->add('receiving', ResponsiblePersonFormType::class, array('role' => 'receiving', 'institutionPerson' => $receivingInstitutionPerson, 'disabled' => $disabled))
                ->getForm();
        }

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $formData = $form->getData();
            $sendingResponsiblePersonData = $formData['sending'];
            $receivingResponsiblePersonData = $formData['receiving'];

            $sendingResponsiblePerson->setInstitutionPerson($sendingResponsiblePersonData['institutionPerson']);
            $receivingInstitutionPerson
                ->setName($receivingResponsiblePersonData['name'])
                ->setPosition($receivingResponsiblePersonData['position'])
                ->setEmail($receivingResponsiblePersonData['email'])
                ->setPhoneNumber($receivingResponsiblePersonData['phoneNumber']);

            $sendingResponsiblePerson->setInstitutionPerson($sendingResponsiblePersonData['institutionPerson']);

            $em->persist($sendingResponsiblePerson);
            $em->persist($receivingResponsiblePerson);
            $em->flush();

            $clickedButton = $form->getClickedButton()->getName();

            if ($clickedButton != 'save' && $clickedButton != null) {
                $redirectService = $this->get('redirect_service');
                if ($user->hasRole('ROLE_STUDENT'))
                    $next = 'learning_agreement_commitment';
                elseif ($user->hasRole('ROLE_ADMIN'))
                    $next = 'learning_agreement_approval_sending';
                else {
                    $next = 'learning_agreement_approval_receiving';
                }
                return $redirectService->buttonRedirectAction($learningAgreement, $clickedButton, 'learning_agreement_mobility_programme', $next);
            }

            return $this->redirectToRoute('learning_agreement_responsible_persons', array(
                'learningAgreement' => $learningAgreement->getId()
            ));
        }

        return $this->render('learning-agreements/responsible_persons.html.twig', array(
            'form' => $form->createView(),
            'learningAgreement' => $learningAgreement
        ));
    }
}