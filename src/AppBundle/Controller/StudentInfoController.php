<?php

namespace AppBundle\Controller;

use AppBundle\Entity\LearningAgreement;
use AppBundle\Entity\User;
use AppBundle\Form\StudentInfoFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @Route("/learning-agreements")
 */
class StudentInfoController extends Controller
{
    /**
     * @Route("/{learningAgreement}/student-info", name="learning_agreement_student_info")
     * @param Request $request
     * @param LearningAgreement $learningAgreement
     * @return Response
     */
    public function studentInfoAction(Request $request, LearningAgreement $learningAgreement)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $this->getUser();

        $beforeMobility = $learningAgreement->getBeforeMobilitySection();
        $authorizationChecker = $this->get('security.authorization_checker');

        if (!$authorizationChecker->isGranted('view', $beforeMobility)) {
            throw new AccessDeniedHttpException();
        }

        $disabled = false;
        if (!$authorizationChecker->isGranted('edit', $beforeMobility)){
            $disabled = true;
        }

        $student = $learningAgreement->getStudent();
        $form = $this->createForm(StudentInfoFormType::class, $student, array('learningAgreement' => $learningAgreement, 'disabled' => $disabled));

        if (!$user->hasRole('ROLE_RECEIVING_ACCOUNT') && $authorizationChecker->isGranted('edit', $beforeMobility)) {
            $form
                ->add('previous', SubmitType::class, array(
                    'validation_groups' => false,
                    'disabled' => true
                ))
                ->add('save', SubmitType::class)
                ->add('next', SubmitType::class, array());
        }
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em->persist($user);

            $learningAgreement->setStudyCycle($form->get('studyCycle')->getData());
            $learningAgreement->setAcademicYear($form->get('academicYear')->getData());
            $learningAgreement->setFieldOfEducation($form->get('fieldOfEducation')->getData());

            $em->flush();

            $clickedButton = $form->getClickedButton()->getName();
            if ($clickedButton == 'next') {
                return $this->redirectToRoute('learning_agreement_sending_institution', array(
                    'learningAgreement' => $learningAgreement->getId()
                ));
            }
        }

        return $this->render('learning-agreements/student_info.html.twig', array(
            'form' => $form->createView(),
            'learningAgreement' => $learningAgreement
        ));
    }
}