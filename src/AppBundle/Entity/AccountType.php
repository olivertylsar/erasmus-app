<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * AccountType
 *
 * @ORM\Table(name="account_type")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AccountTypeRepository")
 */
class AccountType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="account_type_name", type="string", length=255)
     */
    private $accountTypeName;

    /**
     * @ORM\OneToMany(targetEntity="User", mappedBy="accountType")
     */
    protected $users;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set accountTypeName
     *
     * @param string $accountTypeName
     *
     * @return AccountType
     */
    public function setAccountTypeName($accountTypeName)
    {
        $this->accountTypeName = $accountTypeName;

        return $this;
    }

    /**
     * Get accountTypeName
     *
     * @return string
     */
    public function getAccountTypeName()
    {
        return $this->accountTypeName;
    }

    /**
     * Get users
     *
     * @return ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }
}

