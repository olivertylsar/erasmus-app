<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * SectionOfAgreement
 *
 * @ORM\Table(name="agreement_section")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AgreementSectionRepository")
 */
class AgreementSection
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_signed_by_student", type="boolean", nullable=true)
     */
    private $isSignedByStudent;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_signed_by_sending", type="boolean", nullable=true)
     */
    private $isSignedBySending;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_signed_by_receiving", type="boolean", nullable=true)
     */
    private $isSignedByReceiving;

    /**
     * @var string
     *
     * @ORM\Column(name="stage", type="string", length=255)
     */
    private $stage;

    /**
     * @var LearningAgreement
     * @ORM\ManyToOne(targetEntity="LearningAgreement", inversedBy="agreementSections")
     * @ORM\JoinColumn(name="learning_agreement_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $learningAgreement;

    /**
     * @ORM\OneToMany(targetEntity="Signature", mappedBy="agreementSection")
     */
    protected $signatures;

    /**
     * @ORM\OneToMany(targetEntity="SelectedComponent", mappedBy="agreementSection")
     */
    protected $selectedComponents;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isSignedByStudent
     *
     * @param boolean $isSignedByStudent
     *
     * @return AgreementSection
     */
    public function setIsSignedByStudent($isSignedByStudent)
    {
        $this->isSignedByStudent = $isSignedByStudent;

        return $this;
    }

    /**
     * Get isSignedByStudent
     *
     * @return bool
     */
    public function getIsSignedByStudent()
    {
        return $this->isSignedByStudent;
    }

    /**
     * Set isSignedBySending
     *
     * @param boolean $isSignedBySending
     *
     * @return AgreementSection
     */
    public function setIsSignedBySending($isSignedBySending)
    {
        $this->isSignedBySending = $isSignedBySending;

        return $this;
    }

    /**
     * Get isSignedBySending
     *
     * @return bool
     */
    public function getIsSignedBySending()
    {
        return $this->isSignedBySending;
    }

    /**
     * Set isSignedByReceiving
     *
     * @param boolean $isSignedByReceiving
     *
     * @return AgreementSection
     */
    public function setIsSignedByReceiving($isSignedByReceiving)
    {
        $this->isSignedByReceiving = $isSignedByReceiving;

        return $this;
    }

    /**
     * Get isSignedByReceiving
     *
     * @return bool
     */
    public function getIsSignedByReceiving()
    {
        return $this->isSignedByReceiving;
    }

    /**
     * Set stage
     *
     * @param string $stage
     *
     * @return AgreementSection
     */
    public function setStage($stage)
    {
        $this->stage = $stage;

        return $this;
    }

    /**
     * Get stage
     *
     * @return string
     */
    public function getStage()
    {
        return $this->stage;
    }

    /**
     * Set learningAgreement
     *
     * @param string $learningAgreement
     *
     * @return AgreementSection
     */
    public function setLearningAgreement($learningAgreement)
    {
        $this->learningAgreement = $learningAgreement;

        return $this;
    }

    /**
     * Get learningAgreement
     *
     * @return LearningAgreement
     */
    public function getLearningAgreement()
    {
        return $this->learningAgreement;
    }

    /**
     * Get signatures
     *
     * @return ArrayCollection
     */
    public function getSignatures()
    {
        return $this->signatures;
    }

    /**
     * Get studentSignature
     *
     * @return Signature
     */
    public function getStudentSignature()
    {
        foreach ($this->signatures as $signature) {
            /** @var Signature $signature */
            if ($signature->getRole() === 'student'){
                return $signature;
            }
        }
        return null;
    }

    /**
     * Get sendingSignature
     *
     * @return Signature
     */
    public function getSendingSignature()
    {
        foreach ($this->signatures as $signature) {
            /** @var Signature $signature */
            if ($signature->getRole() === 'sending'){
                return $signature;
            }
        }
        return null;
    }

    /**
     * Get receivingSignature
     *
     * @return Signature
     */
    public function getReceivingSignature()
    {
        foreach ($this->signatures as $signature) {
            /** @var Signature $signature */
            if ($signature->getRole() === 'receiving'){
                return $signature;
            }
        }
        return null;
    }

    /**
     * Get SelectedComponents
     *
     * @return ArrayCollection
     */
    public function getSelectedComponents()
    {
        return $this->selectedComponents;
    }

    /**
     * Get isFullySigned
     *
     * @return boolean
     */
    public function isFullySigned()
    {
        if ($this->isSignedByStudent && $this->isSignedBySending && $this->isSignedByReceiving){
            return true;
        }
        return false;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        if ($this->isSignedByStudent){
            if ($this->isSignedBySending){
                if ($this->isSignedByReceiving){
                    return 'Approved By Both';
                }
                return 'Approved By Sending';
            }
            return 'Committed';
        }
        return 'Editable';
    }

}

