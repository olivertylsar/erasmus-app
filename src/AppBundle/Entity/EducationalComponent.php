<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * EducationalComponent
 *
 * @ORM\Table(name="educational_component")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EducationalComponentRepository")
 */
class EducationalComponent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="component_code", type="string", length=255)
     */
    private $componentCode;

    /**
     * @var string
     *
     * @ORM\Column(name="component_title", type="string", length=255)
     */
    private $componentTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="semester", type="string", length=255)
     */
    private $semester;

    /**
     * @var int
     *
     * @ORM\Column(name="ects_credits", type="integer")
     */
    private $ectsCredits;

    /**
     * @var Institution
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Institution", inversedBy="educationalComponents")
     * @ORM\JoinColumn(name="institution_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $institution;

    /**
     * @ORM\OneToMany(targetEntity="SelectedComponent", mappedBy="educationalComponent")
     */
    protected $selectedComponents;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set componentCode
     *
     * @param string $componentCode
     *
     * @return EducationalComponent
     */
    public function setComponentCode($componentCode)
    {
        $this->componentCode = $componentCode;

        return $this;
    }

    /**
     * Get componentCode
     *
     * @return string
     */
    public function getComponentCode()
    {
        return $this->componentCode;
    }

    /**
     * Set componentTitle
     *
     * @param string $componentTitle
     *
     * @return EducationalComponent
     */
    public function setComponentTitle($componentTitle)
    {
        $this->componentTitle = $componentTitle;

        return $this;
    }

    /**
     * Get componentTitle
     *
     * @return string
     */
    public function getComponentTitle()
    {
        return $this->componentTitle;
    }

    /**
     * Set semester
     *
     * @param string $semester
     *
     * @return EducationalComponent
     */
    public function setSemester($semester)
    {
        $this->semester = $semester;

        return $this;
    }

    /**
     * Get semester
     *
     * @return string
     */
    public function getSemester()
    {
        return $this->semester;
    }

    /**
     * Set ectsCredits
     *
     * @param integer $ectsCredits
     *
     * @return EducationalComponent
     */
    public function setEctsCredits($ectsCredits)
    {
        $this->ectsCredits = $ectsCredits;

        return $this;
    }

    /**
     * Get ectsCredits
     *
     * @return int
     */
    public function getEctsCredits()
    {
        return $this->ectsCredits;
    }

    /**
     * Set institution
     *
     * @param string $institution
     *
     * @return EducationalComponent
     */
    public function setInstitution($institution)
    {
        $this->institution = $institution;

        return $this;
    }

    /**
     * Get Institution
     *
     * @return string
     */
    public function getInstitution()
    {
        return $this->institution;
    }

    /**
     * Get selectedComponents
     *
     * @return ArrayCollection
     */
    public function getSelectedComponents()
    {
        return $this->selectedComponents;
    }
}

