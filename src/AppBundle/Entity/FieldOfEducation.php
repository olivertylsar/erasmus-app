<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * FieldOfEducation
 *
 * @ORM\Table(name="field_of_education")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FieldOfEducationRepository")
 */
class FieldOfEducation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="field_code", type="string", length=255)
     */
    private $fieldCode;

    /**
     * @var string
     *
     * @ORM\Column(name="field_description", type="string", length=255)
     */
    private $fieldDescription;

    /**
     * @ORM\OneToMany(targetEntity="LearningAgreement", mappedBy="fieldOfEducation")
     */
    protected $learningAgreements;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fieldCode
     *
     * @param string $fieldCode
     *
     * @return FieldOfEducation
     */
    public function setFieldCode($fieldCode)
    {
        $this->fieldCode = $fieldCode;

        return $this;
    }

    /**
     * Get fieldCode
     *
     * @return string
     */
    public function getFieldCode()
    {
        return $this->fieldCode;
    }

    /**
     * Set fieldDescription
     *
     * @param string $fieldDescription
     *
     * @return FieldOfEducation
     */
    public function setFieldDescription($fieldDescription)
    {
        $this->fieldDescription = $fieldDescription;

        return $this;
    }

    /**
     * Get fieldDescription
     *
     * @return string
     */
    public function getFieldDescription()
    {
        return $this->fieldDescription;
    }

    /**
     * Get learningAgreements
     *
     * @return ArrayCollection
     */
    public function getLearningAgreements()
    {
        return $this->learningAgreements;
    }

    /**
     * Get choiceLabel
     *
     * @return string
     */
    public function getChoiceLabel()
    {
        return $this->fieldCode . ' - ' . $this->fieldDescription;
    }
}

