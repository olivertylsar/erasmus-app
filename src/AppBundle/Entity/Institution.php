<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Institution
 *
 * @ORM\Table(name="institution")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\InstitutionRepository")
 */
class Institution
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="erasmusCode", type="string", length=255)
     */
    private $erasmusCode;

    /**
     * @var string
     *
     * @ORM\Column(name="website", type="string", length=255)
     */
    private $website;

    /**
     * @var Country
     * @ORM\ManyToOne(targetEntity="Country", inversedBy="institutions")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $country;

    /**
     * @ORM\OneToMany(targetEntity="InvolvedInstitution", mappedBy="institution")
     */
    protected $involvedInstitutions;

    /**
     * @ORM\OneToMany(targetEntity="EducationalComponent", mappedBy="institution")
     */
    protected $educationalComponents;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\InstitutionPerson", mappedBy="institution")
     */
    protected $institutionPersons;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Institution
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Institution
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set erasmusCode
     *
     * @param string $erasmusCode
     *
     * @return Institution
     */
    public function setErasmusCode($erasmusCode)
    {
        $this->erasmusCode = $erasmusCode;

        return $this;
    }

    /**
     * Get erasmusCode
     *
     * @return string
     */
    public function getErasmusCode()
    {
        return $this->erasmusCode;
    }

    /**
     * Set website
     *
     * @param string $website
     *
     * @return Institution
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return Institution
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Get involvedInstitutions
     *
     * @return ArrayCollection
     */
    public function getInvolvedInstitutions()
    {
        return $this->involvedInstitutions;
    }

    /**
     * Get educationalComponents
     *
     * @return ArrayCollection
     */
    public function getEducationalComponents()
    {
        return $this->educationalComponents;
    }

    /**
     * Get institutionPersons
     *
     * @return ArrayCollection
     */
    public function getInstitutionPersons()
    {
        return $this->institutionPersons;
    }
}

