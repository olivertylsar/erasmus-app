<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * InstitutionInAgreement
 *
 * @ORM\Table(name="involved_institution")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\InvolvedInstitutionRepository")
 */
class InvolvedInstitution
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="role_in_agreement", type="string", length=255)
     */
    private $roleInAgreement;

    /**
     * @var string
     *
     * @ORM\Column(name="faculty", type="string", length=255, nullable=true)
     */
    private $faculty;

    /**
     * @var string
     *
     * @ORM\Column(name="department", type="string", length=255, nullable=true)
     */
    private $department;

    /**
     * @var string
     *
     * @ORM\Column(name="course_catalog_link", type="string", length=255, nullable=true)
     */
    private $courseCatalogueLink;

    /**
     * @var LearningAgreement
     * @ORM\ManyToOne(targetEntity="LearningAgreement", inversedBy="involvedInstitutions")
     * @ORM\JoinColumn(name="learning_agreement_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $learningAgreement;

    /**
     * @var Institution
     * @ORM\ManyToOne(targetEntity="Institution", fetch="EAGER", inversedBy="involvedInstitutions")
     * @ORM\JoinColumn(name="institution_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $institution;

    /**
     * @ORM\OneToMany(targetEntity="SelectedComponent", mappedBy="involvedInstitution")
     */
    protected $selectedComponents;

    /**
     * @ORM\OneToMany(targetEntity="InvolvedPerson", mappedBy="involvedInstitution")
     */
    protected $involvedPersons;

    public function __construct(LearningAgreement $learningAgreement, $roleInAgreement)
    {
        $this->learningAgreement = $learningAgreement;
        $this->roleInAgreement = $roleInAgreement;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set roleInAgreement
     *
     * @param string $roleInAgreement
     *
     * @return InvolvedInstitution
     */
    public function setRoleInAgreement($roleInAgreement)
    {
        $this->roleInAgreement = $roleInAgreement;

        return $this;
    }

    /**
     * Get roleInAgreement
     *
     * @return string
     */
    public function getRoleInAgreement()
    {
        return $this->roleInAgreement;
    }

    /**
     * Set faculty
     *
     * @param string $faculty
     *
     * @return InvolvedInstitution
     */
    public function setFaculty($faculty)
    {
        $this->faculty = $faculty;

        return $this;
    }

    /**
     * Get faculty
     *
     * @return string
     */
    public function getFaculty()
    {
        return $this->faculty;
    }

    /**
     * Set department
     *
     * @param string $department
     *
     * @return InvolvedInstitution
     */
    public function setDepartment($department)
    {
        $this->department = $department;

        return $this;
    }

    /**
     * Get department
     *
     * @return string
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Set courseCatalogueLink
     *
     * @param string $courseCatalogueLink
     *
     * @return InvolvedInstitution
     */
    public function setCourseCatalogueLink($courseCatalogueLink)
    {
        $this->courseCatalogueLink = $courseCatalogueLink;

        return $this;
    }

    /**
     * Get courseCatalogueLink
     *
     * @return string
     */
    public function getCourseCatalogueLink()
    {
        return $this->courseCatalogueLink;
    }

    /**
     * Set learningAgreement
     *
     * @param string $learningAgreement
     *
     * @return InvolvedInstitution
     */
    public function setLearningAgreement($learningAgreement)
    {
        $this->learningAgreement = $learningAgreement;

        return $this;
    }

    /**
     * Get learningAgreement
     *
     * @return string
     */
    public function getLearningAgreement()
    {
        return $this->learningAgreement;
    }

    /**
     * Set institution
     *
     * @param string $institution
     *
     * @return InvolvedInstitution
     */
    public function setInstitution($institution)
    {
        $this->institution = $institution;

        return $this;
    }

    /**
     * Get Institution
     *
     * @return string
     */
    public function getInstitution()
    {
        return $this->institution;
    }

    /**
     * Get selectedComponents
     *
     * @return ArrayCollection
     */
    public function getSelectedComponents()
    {
        return $this->selectedComponents;
    }

    /**
     * Get beforeMobilityComponents
     *
     * @return ArrayCollection
     */
    public function getBeforeMobilityComponents()
    {
        $selectedComponents = $this->selectedComponents;
        $beforeMobilityComponents = array();
        foreach ($selectedComponents as $component) {
            /** @var SelectedComponent $component */
            if ($component->getAgreementSection()->getStage() === 'before') { //return only those components that were added before mobility
                $beforeMobilityComponents[] = $component;
            }
        }
        return new ArrayCollection($beforeMobilityComponents);
    }

    /**
     * Get afterMobilityComponents
     *
     * @return ArrayCollection
     */
    public function getDuringMobilityComponents()
    {
        $selectedComponents = $this->selectedComponents;
        $duringMobilityComponents = array();
        foreach ($selectedComponents as $component) {
            /** @var SelectedComponent $component */
            if ($component->getAgreementSection()->getStage() === 'during') { //return only those components that were added during mobility
                $duringMobilityComponents[] = $component;
            }
        }
        return new ArrayCollection($duringMobilityComponents);
    }

    /**
     * Get involvedPersons
     *
     * @return ArrayCollection
     */
    public function getInvolvedPersons()
    {
        return $this->involvedPersons;
    }

    /**
     * Get responsiblePerson
     *
     * @return InvolvedPerson
     */
    public function getResponsiblePerson()
    {
        foreach ($this->involvedPersons as $involvedPerson) {
            /** @var InvolvedPerson $involvedPerson */
            if ($involvedPerson->getPersonRole() == 'responsible') {
                return $involvedPerson;
            }
        }
        return null;
    }

    /**
     * Get contactPerson
     *
     * @return InvolvedPerson
     */
    public function getContactPerson()
    {
        foreach ($this->involvedPersons as $involvedPerson) {
            /** @var InvolvedPerson $involvedPerson */
            if ($involvedPerson->getPersonRole() == 'contact') {
                return $involvedPerson;
            }
        }
        return null;
    }
}

