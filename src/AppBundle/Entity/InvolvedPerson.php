<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InvolvedPerson
 *
 * @ORM\Table(name="involved_person")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\InvolvedPersonRepository")
 */
class InvolvedPerson
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="person_role", type="string", length=255)
     */
    private $personRole;

    /**
     * @var InvolvedInstitution
     * @ORM\ManyToOne(targetEntity="InvolvedInstitution", inversedBy="involvedPersons")
     * @ORM\JoinColumn(name="involved_institution_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $involvedInstitution;

    /**
     * @var InstitutionPerson
     * @ORM\ManyToOne(targetEntity="InstitutionPerson", fetch="EAGER", inversedBy="involvedPersons")
     * @ORM\JoinColumn(name="institution_person_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $institutionPerson;

    public function __construct($personRole)
    {
        $this->personRole = $personRole;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set personRole
     *
     * @param string $personRole
     *
     * @return InvolvedPerson
     */
    public function setPersonRole($personRole)
    {
        $this->personRole = $personRole;

        return $this;
    }

    /**
     * Get personRole
     *
     * @return string
     */
    public function getPersonRole()
    {
        return $this->personRole;
    }

    /**
     * Set institutionPerson
     *
     * @param InstitutionPerson $institutionPerson
     *
     * @return InvolvedPerson
     */
    public function setInstitutionPerson($institutionPerson)
    {
        $this->institutionPerson = $institutionPerson;

        return $this;
    }

    /**
     * Get InstitutionPerson
     *
     * @return InstitutionPerson
     */
    public function getInstitutionPerson()
    {
        return $this->institutionPerson;
    }

    /**
     * Set involvedInstitution
     *
     * @param InvolvedInstitution $involvedInstitution
     *
     * @return InvolvedPerson
     */
    public function setInvolvedInstitution($involvedInstitution)
    {
        $this->involvedInstitution = $involvedInstitution;

        return $this;
    }

    /**
     * Get InvolvedInstitution
     *
     * @return string
     */
    public function getInvolvedInstitution()
    {
        return $this->involvedInstitution;
    }
}

