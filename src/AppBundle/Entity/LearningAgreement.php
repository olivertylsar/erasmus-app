<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * LearningAgreement
 *
 * @ORM\Table(name="learning_agreement")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LearningAgreementRepository")
 */
class LearningAgreement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="academic_year", type="string", nullable=true, length=10)
     */
    private $academicYear;

    /**
     * @var string
     *
     * @ORM\Column(name="study_cycle", type="string", nullable=true, length=255)
     */
    private $studyCycle;

    /**
     * @var string
     *
     * @ORM\Column(name="language_competence", type="string", nullable=true, length=20)
     */
    private $languageCompetence;

    /**
     * @ORM\Column(name="start_date", type="date", nullable=true)
     */
    private $startDate;

    /**
     * @ORM\Column(name="end_date", type="date", nullable=true)
     */
    private $endDate;

    /**
     * @var Language
     * @ORM\ManyToOne(targetEntity="Language", inversedBy="learningAgreements")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $language;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="learningAgreements")
     * @ORM\JoinColumn(name="student_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $student;

    /**
     * @var FieldOfEducation
     * @ORM\ManyToOne(targetEntity="FieldOfEducation", inversedBy="learningAgreements")
     * @ORM\JoinColumn(name="field_of_education_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $fieldOfEducation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity="InvolvedInstitution", mappedBy="learningAgreement")
     */
    protected $involvedInstitutions;

    /**
     * @ORM\OneToMany(targetEntity="AgreementSection", mappedBy="learningAgreement")
     */
    protected $agreementSections;

    public function __construct()
    {
        $this->createdAt = new \DateTime('now', new \DateTimeZone('Europe/Lisbon'));
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set academicYear
     *
     * @param string $academicYear
     *
     * @return LearningAgreement
     */
    public function setAcademicYear($academicYear)
    {
        $this->academicYear = $academicYear;

        return $this;
    }

    /**
     * Get academicYear
     *
     * @return string
     */
    public function getAcademicYear()
    {
        return $this->academicYear;
    }

    /**
     * Set studyCycle
     *
     * @param string $studyCycle
     *
     * @return LearningAgreement
     */
    public function setStudyCycle($studyCycle)
    {
        $this->studyCycle = $studyCycle;

        return $this;
    }

    /**
     * Get studyCycle
     *
     * @return string
     */
    public function getStudyCycle()
    {
        return $this->studyCycle;
    }

    /**
     * Set languageCompetence
     *
     * @param string $languageCompetence
     *
     * @return LearningAgreement
     */
    public function setLanguageCompetence($languageCompetence)
    {
        $this->languageCompetence = $languageCompetence;

        return $this;
    }

    /**
     * Get languageCompetence
     *
     * @return string
     */
    public function getLanguageCompetence()
    {
        return $this->languageCompetence;
    }

    /**
     * Set startDate
     *
     * @param string $startDate
     *
     * @return LearningAgreement
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param string $endDate
     *
     * @return LearningAgreement
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set language
     *
     * @param string $language
     *
     * @return LearningAgreement
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set student
     *
     * @param string $student
     *
     * @return LearningAgreement
     */
    public function setStudent($student)
    {
        $this->student = $student;

        return $this;
    }

    /**
     * Get student
     *
     * @return User
     */
    public function getStudent()
    {
        return $this->student;
    }

    /**
     * Set fieldOfEducation
     *
     * @param string $fieldOfEducation
     *
     * @return LearningAgreement
     */
    public function setFieldOfEducation($fieldOfEducation)
    {
        $this->fieldOfEducation = $fieldOfEducation;

        return $this;
    }

    /**
     * Get fieldOfEducation
     *
     * @return string
     */
    public function getFieldOfEducation()
    {
        return $this->fieldOfEducation;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return LearningAgreement
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get involvedInstitutions
     *
     * @return ArrayCollection
     */
    public function getInvolvedInstitutions()
    {
        return $this->involvedInstitutions;
    }

    /**
     * Get agreementSections
     *
     * @return ArrayCollection
     */
    public function getAgreementSections()
    {
        return $this->agreementSections;
    }

    /**
     * Get involvedInstitution
     *
     * @return InvolvedInstitution
     */
    public function getSendingInstitution()
    {
        foreach ($this->getInvolvedInstitutions() as $involvedInstitution) {
            /** @var InvolvedInstitution $involvedInstitution */
            if ($involvedInstitution->getRoleInAgreement() == 'sending') {
                return $involvedInstitution;
            }
        }
        return null;
    }

    /**
     * Get receivingInstitution
     *
     * @return InvolvedInstitution
     */
    public function getReceivingInstitution()
    {
        foreach ($this->getInvolvedInstitutions() as $involvedInstitution) {
            /** @var InvolvedInstitution $involvedInstitution */
            if ($involvedInstitution->getRoleInAgreement() == 'receiving') {
                return $involvedInstitution;
            }
        }
        return null;
    }

    /**
     * Get isResponsiblePersonLoggedIn
     *
     * @return boolean
     */
    public function isResponsiblePersonLoggedIn(User $user)
    {
        $responsiblePersonEmail = $this->getReceivingInstitution()->getResponsiblePerson()->getInstitutionPerson()->getEmail();
        return ($responsiblePersonEmail === $user->getEmail()) ? : false;
    }

    /**
     * Get beforeMobilitySection
     *
     * @return AgreementSection
     */
    public function getBeforeMobilitySection()
    {
        foreach ($this->getAgreementSections() as $agreementSection){
            /** @var AgreementSection $agreementSection */
            if ($agreementSection->getStage() === 'before'){
                return $agreementSection;
            }
        }
        return null;
    }

    /**
     * Get duringMobilitySection
     *
     * @return AgreementSection
     */
    public function getDuringMobilitySection()
    {
        foreach ($this->getAgreementSections() as $agreementSection){
            /** @var AgreementSection $agreementSection */
            if ($agreementSection->getStage() === 'during'){
                return $agreementSection;
            }
        }
        return null;
    }

}

