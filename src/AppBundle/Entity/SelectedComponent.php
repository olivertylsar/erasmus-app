<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SelectedComponent
 *
 * @ORM\Table(name="selected_component")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SelectedComponentRepository")
 */
class SelectedComponent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reason", type="string", length=255, nullable=true)
     */
    private $reason;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_deleted", type="boolean", nullable=true)
     */
    private $isDeleted;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_added", type="boolean", nullable=true)
     */
    private $isAdded;

    /**
     * @var EducationalComponent
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\EducationalComponent", inversedBy="selectedComponents")
     * @ORM\JoinColumn(name="educational_component_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $educationalComponent;

    /**
     * @var InvolvedInstitution
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\InvolvedInstitution", inversedBy="selectedComponents")
     * @ORM\JoinColumn(name="involved_institution_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $involvedInstitution;

    /**
     * @var AgreementSection
     * @ORM\ManyToOne(targetEntity="AgreementSection", inversedBy="selectedComponents")
     * @ORM\JoinColumn(name="agreement_section_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $agreementSection;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reason
     *
     * @param string $reason
     *
     * @return SelectedComponent
     */
    public function setReason($reason)
    {
        $this->reason = $reason;

        return $this;
    }

    /**
     * Get reason
     *
     * @return string
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     *
     * @return SelectedComponent
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Set isAdded
     *
     * @param boolean $isAdded
     *
     * @return SelectedComponent
     */
    public function setIsAdded($isAdded)
    {
        $this->isAdded = $isAdded;

        return $this;
    }

    /**
     * Get isAdded
     *
     * @return boolean
     */
    public function getIsAdded()
    {
        return $this->isAdded;
    }

    /**
     * Set educationalComponent
     *
     * @param string $educationalComponent
     *
     * @return SelectedComponent
     */
    public function setEducationalComponent($educationalComponent)
    {
        $this->educationalComponent = $educationalComponent;

        return $this;
    }

    /**
     * Get EducationalComponent
     *
     * @return string
     */
    public function getEducationalComponent()
    {
        return $this->educationalComponent;
    }

    /**
     * Set involvedInstitution
     *
     * @param string $involvedInstitution
     *
     * @return SelectedComponent
     */
    public function setInvolvedInstitution($involvedInstitution)
    {
        $this->involvedInstitution = $involvedInstitution;

        return $this;
    }

    /**
     * Get InvolvedInstitution
     *
     * @return InvolvedInstitution
     */
    public function getInvolvedInstitution()
    {
        return $this->involvedInstitution;
    }

    /**
     * Get isBefore
     *
     * @return boolean
     */
    public function isBefore()
    {
        if ($this->getAgreementSection()->getStage() === 'before') {
            return true;
        }
        return false;
    }

    /**
     * Get isSending
     *
     * @return boolean
     */
    public function isSending()
    {
        if ($this->getInvolvedInstitution()->getRoleInAgreement() === 'sending') {
            return true;
        }
        return false;
    }

    /**
     * Set agreementSection
     *
     * @param AgreementSection $agreementSection
     *
     * @return SelectedComponent
     */
    public function setAgreementSection($agreementSection)
    {
        $this->agreementSection = $agreementSection;

        return $this;
    }

    /**
     * Get agreementSection
     *
     * @return AgreementSection
     */
    public function getAgreementSection()
    {
        return $this->agreementSection;
    }
}

