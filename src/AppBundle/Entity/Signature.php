<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Signature
 *
 * @ORM\Table(name="signature")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SignatureRepository")
 */
class Signature
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=255)
     */
    private $filename;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="signed_at", type="datetime")
     */
    private $signedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=255)
     */
    private $role;

    /**
     * @var string
     *
     * @ORM\Column(name="is_digital", type="boolean")
     */
    private $isDigital;

    /**
     * @var AgreementSection
     * @ORM\ManyToOne(targetEntity="AgreementSection", inversedBy="signatures")
     * @ORM\JoinColumn(name="agreement_section_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $agreementSection;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set filename
     *
     * @param string $filename
     *
     * @return Signature
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set signedAt
     *
     * @param \DateTime $signedAt
     *
     * @return Signature
     */
    public function setSignedAt($signedAt)
    {
        $this->signedAt = $signedAt;

        return $this;
    }

    /**
     * Get signedAt
     *
     * @return \DateTime
     */
    public function getSignedAt()
    {
        return $this->signedAt;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return Signature
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set isDigital
     *
     * @param boolean $isDigital
     *
     * @return Signature
     */
    public function setIsDigital($isDigital)
    {
        $this->isDigital = $isDigital;

        return $this;
    }

    /**
     * Get isDigital
     *
     * @return boolean
     */
    public function getIsDigital()
    {
        return $this->isDigital;
    }

    /**
     * Set agreementSection
     *
     * @param string $agreementSection
     *
     * @return Signature
     */
    public function setAgreementSection($agreementSection)
    {
        $this->agreementSection = $agreementSection;

        return $this;
    }

    /**
     * Get agreementSection
     *
     * @return AgreementSection
     */
    public function getAgreementSection()
    {
        return $this->agreementSection;
    }
}

