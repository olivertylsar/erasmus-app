<?php

namespace AppBundle\Form;

use AppBundle\Entity\EducationalComponent;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ComponentFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $disabled = $options['disable'];
        $builder
            ->add('componentCode', TextType::class, array(
                'required' => false,
                'empty_data' => '-',
                'disabled' => $disabled
            ))
            ->add('componentTitle', TextType::class, array(
                'disabled' => $disabled
            ))
            ->add('semester', ChoiceType::class, array(
                'choices' => array(
                    'First semester (Winter/Autumn)' => 'First semester',
                    'Second semester (Summer/Spring)' => 'Second semester',
                    'First trimester' => 'First trimester',
                    'Second trimester' => 'Second trimester',
                    'Third trimester' => 'Second semester',
                    'Full academic year' => 'Second semester',
                ),
                'placeholder' => '---',
                'disabled' => $disabled
            ))
            ->add('ectsCredits', NumberType::class, array(
                'label' => 'ECTS Credits',
                'disabled' => $disabled
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => EducationalComponent::class,
            'disable' => false
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_component_form_type';
    }
}
