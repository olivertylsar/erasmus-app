<?php

namespace AppBundle\Form;

use AppBundle\Entity\Country;
use AppBundle\Entity\Institution;
use AppBundle\Entity\InstitutionPerson;
use AppBundle\Entity\InvolvedInstitution;
use AppBundle\Entity\InvolvedPerson;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InvolvedInstitutionFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $role = $options['role'];

        /** @var InvolvedInstitution $involvedInstitution */
        $involvedInstitution = $options['involved_institution'];

        /** @var Institution $institution */
        $institution = $involvedInstitution->getInstitution();

        /** @var InvolvedPerson $contactPerson */
        $contactPerson = $involvedInstitution->getContactPerson() ? : null;
        $contactPersonData = $contactPerson ? $contactPerson->getInstitutionPerson() : null;

        if ($institution) {
            $institutionAddress = $institution->getAddress();
            $institutionCountry = $institution->getCountry();
            $institutionErasmusCode = $institution->getErasmusCode();
        }
        else {
            $institutionAddress = $institutionCountry = $institutionErasmusCode = null;
        }

        if ($contactPersonData) {
            $contactName = $contactPersonData->getName();
            $contactEmail = $contactPersonData->getEmail();
            $contactPhone = $contactPersonData->getPhoneNumber();
        }
        else {
            $contactName = $contactEmail = $contactPhone = null;
        }

        if ($role == 'receiving'){
            $builder
                ->add('institution', EntityType::class, array(
                    'class' => Institution::class,
                    'choice_label' => 'name',
                    'data' => $institution,
                    'placeholder' => '---'
                ))
                ->add('address', TextType::class, array(
                    'disabled' => true,
                    'mapped' => false,
                    'data' => $institutionAddress,
                ))
                ->add('country', EntityType::class, array(
                    'class' => Country::class,
                    'choice_label' => 'countryName',
                    'placeholder' => '---',
                    'disabled' => true,
                    'mapped' => false,
                    'data' => $institutionCountry,
                ))
                ->add('erasmusCode', TextType::class, array(
                    'disabled' => true,
                    'mapped' => false,
                    'data' => $institutionErasmusCode,
                ))
                ->add('faculty', TextType::class, array(
                    'disabled' => false
                ))
                ->add('department', TextType::class, array(
                    'disabled' => false
                ))
                ->add('contactPersonName', TextType::class, array(
                    'mapped' => false,
                    'data' => $contactName
                ))
                ->add('contactPersonEmail', TextType::class, array(
                    'mapped' => false,
                    'data' => $contactEmail
                ))
                ->add('contactPersonPhone', TextType::class, array(
                    'mapped' => false,
                    'data' => $contactPhone
                ))
            ;
        }
        else { //sending
            $builder
                ->add('institution', EntityType::class, array(
                    'class' => Institution::class,
                    'choice_label' => 'name',
                    'data' => $institution,
                    'disabled' => true
                ))
                ->add('address', TextType::class, array(
                    'disabled' => true,
                    'mapped' => false,
                    'data' => $institution->getAddress(),
                ))
                ->add('country', EntityType::class, array(
                    'class' => Country::class,
                    'choice_label' => 'countryName',
                    'placeholder' => '---',
                    'disabled' => true,
                    'mapped' => false,
                    'data' => $institution->getCountry(),
                ))
                ->add('erasmusCode', TextType::class, array(
                    'disabled' => true,
                    'mapped' => false,
                    'data' => $institution->getErasmusCode(),
                ))
                ->add('faculty', TextType::class, array(
                    'disabled' => false
                ))
                ->add('department', TextType::class, array(
                    'disabled' => false
                ))
                ->add('contactPerson', EntityType::class, array(
                    'class' => InstitutionPerson::class,
                    'choice_label' => 'name',
                    'disabled' => false,
                    'mapped' => false,
                    'placeholder' => '---',
                    'data' => $contactPersonData,
                    'query_builder' => function(EntityRepository $er ) use ( $options ) {
                        return $er->createQueryBuilder('ip')
                            ->where('ip.institution = :upt')
                            ->setParameter('upt', 1);
                    }
                ))
                ->add('contactPersonEmail', TextType::class, array(
                    'mapped' => false,
                    'data' => $contactEmail,
                    'disabled' => true
                ))
                ->add('contactPersonPhone', TextType::class, array(
                    'mapped' => false,
                    'data' => $contactPhone,
                    'disabled' => true
                ))
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => InvolvedInstitution::class,
            'role' => null,
            'involved_institution' => null,
            'dis' => false
        ));
    }
}