<?php

namespace AppBundle\Form;

use AppBundle\Entity\Language;
use AppBundle\Entity\LearningAgreement;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MobilityProgrammeFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $involvedInstitutions = $options['involved_institutions'];

        $builder
            ->add('receivingCourseCatalogueLink', TextType::class, array(
                'mapped' => false,
                'label' => 'Link to course catalogue',
                'data' => $involvedInstitutions['receiving']->getCourseCatalogueLink()
            ))
            ->add('sendingCourseCatalogueLink', TextType::class, array(
                'mapped' => false,
                'label' => 'Link to course catalogue',
                'data' => $involvedInstitutions['sending']->getCourseCatalogueLink()
            ))
            ->add('startDate', DateType::class, array(
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'html5' => false
            ))
            ->add('endDate', DateType::class, array(
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'html5' => false
            ))
            ->add('language', EntityType::class, array(
                'class' => Language::class,
                'choice_label' => 'language_name',
                'placeholder' => '---'
            ))
            ->add('languageCompetence', ChoiceType::class, array(
                'choices' => array(
                    'A1' => 'A1',
                    'A2' => 'A2',
                    'B1' => 'B1',
                    'B2' => 'B2',
                    'C1' => 'C1',
                    'C2' => 'C2',
                    'Native Speaker' => 'Native Speaker'
                ),
                'placeholder' => '---'
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => LearningAgreement::class,
            'involved_institutions' => null
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_mobility_programme_form_type';
    }
}
