<?php

namespace AppBundle\Form;

use AppBundle\Entity\InstitutionPerson;
use AppBundle\Entity\InvolvedPerson;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ResponsiblePersonFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $role = $options['role'];
        /** @var InstitutionPerson $institutionPerson */
        $institutionPerson = $options['institutionPerson'];
        if ($institutionPerson) {
            $name = $institutionPerson->getName();
            $position = $institutionPerson->getPosition();
            $email = $institutionPerson->getEmail();
            $number = $institutionPerson->getPhoneNumber();
        }
        else {
            $position = $email = $number = null;
        }

        if ($role == 'sending') {

            $builder
                ->add('institutionPerson', EntityType::class, array(
                    'class' => InstitutionPerson::class,
                    'choice_label' => 'name',
                    'label' => 'Name',
                    'placeholder' => '---',
                    'data' => $institutionPerson,
                    'query_builder' => function(EntityRepository $er ) use ( $options ) {
                        return $er->createQueryBuilder('ip')
                            ->where('ip.institution = :upt')
                            ->setParameter('upt', 1);
                    }
                    ))
                ->add('position', TextType::class, array(
                    'disabled' => true,
                    'data' => $position
                    ))
                ->add('email', TextType::class, array(
                    'disabled' => true,
                    'data' => $email
                ))
                ->add('phoneNumber', TextType::class, array(
                    'disabled' => true,
                    'data' => $number
                ))
                ;
        }
        else {
            $builder
                ->add('name', TextType::class, array(
                    'data' => $name
                ))
                ->add('position', TextType::class, array(
                    'data' => $position
                ))
                ->add('email', TextType::class, array(
                    'data' => $email
                ))
                ->add('phoneNumber', TextType::class, array(
                    'data' => $number
                ))
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'role' => null,
            'institutionPerson' => null
        ));
    }

}
