<?php

namespace AppBundle\Form;

use AppBundle\Entity\Country;
use AppBundle\Entity\FieldOfEducation;
use AppBundle\Entity\LearningAgreement;
use AppBundle\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StudentInfoFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var LearningAgreement $learningAgreement */
        $learningAgreement = $options['learningAgreement'];

        $builder
            ->add('firstName', TextType::class)
            ->add('lastName', TextType::class)
            ->add('dateOfBirth', DateType::class, array(
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'html5' => false
            ))
            ->add('country', EntityType::class, array(
                'class' => Country::class,
                'choice_label' => 'country_name',
                'placeholder' => '---'
            ))
            ->add('sex', ChoiceType::class, array(
                'choices' => array(
                    'Male' => 'M',
                    'Female' => 'F'
                ),
                'multiple' => false,
                'expanded' => true
            ))
            ->add('universityIdNumber', TextType::class, array(
                'label' => 'University ID Number'
            ))
            ->add('phoneNumber', TextType::class)
            ->add('studyCycle', ChoiceType::class, array(
                'data' => $learningAgreement->getStudyCycle(),
                'mapped' => false,
                'placeholder' => '---',
                'choices' => array(
                    'Short cycle or equivalent (EQF 5)' => 'EQF 5',
                    'Bachelor or equivalent first cycle (EQF 6)'=> 'EQF 6',
                    'Master or equivalent second cycle (EQF 7)' => 'EQF 7',
                    'Doctorate or equivalent (EQF 8)' => 'EQF 8'
                )
            ))
            ->add('academicYear', ChoiceType::class, array(
                'data' => $learningAgreement->getAcademicYear(),
                'mapped' => false,
                'placeholder' => '---',
                'choices' => array(
                    '2018/2019' => '2018/2019' ,
                    '2019/2020' => '2019/2020',
                    '2020/2021' => '2020/2021',
                    '2021/2022' => '2021/2022',
                    '2022/2023' => '2022/2023'
                )
            ))
            ->add('fieldOfEducation', EntityType::class, array(
                'data' => $learningAgreement->getFieldOfEducation(),
                'mapped' => false,
                'placeholder' => '---',
                'class' => FieldOfEducation::class,
                'choice_label' => 'choiceLabel'
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
            'learningAgreement' => null
        ));
    }
}