<?php

namespace AppBundle\Security;

use AppBundle\Entity\AgreementSection;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class AgreementSectionVoter extends Voter
{
    const EDIT = 'edit';
    const REMOVE = 'remove';
    const VIEW = 'view';
    const EXPORT = 'export';
    const CREATE_DURING = 'createDuring';
    const SIGN = 'sign';

    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    /**
     * @param string $attribute
     * @param mixed $subject
     * @return bool
     */
    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, array(self::EDIT, self::REMOVE, self::VIEW, self::EXPORT, self::CREATE_DURING, self::SIGN))) {
            return false;
        }

        // only vote on LearningAgreement objects inside this voter
        if (!$subject instanceof AgreementSection) {
            return false;
        }

        return true;
    }

    /**
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        // you know $subject is a AgreementSection object, thanks to supports
        /** @var AgreementSection $agreementSection */
        $agreementSection = $subject;

        switch ($attribute) {
            case self::EDIT:
                return $this->canEdit($agreementSection, $user, $token);
            case self::REMOVE:
                return $this->canRemove($agreementSection, $user, $token);
            case self::VIEW:
                return $this->canView($agreementSection, $user, $token);
            case self::EXPORT:
                return $this->canExport($agreementSection, $user);
            case self::CREATE_DURING:
                return $this->canCreateDuring($agreementSection, $user, $token);
            case self::SIGN:
                return $this->canSign($agreementSection, $user, $token);
        }

        return false;
    }

    /**
     * @param AgreementSection $agreementSection
     * @param User $user
     * @param TokenInterface $token
     * @return bool
     */
    private function canEdit(AgreementSection $agreementSection, User $user, TokenInterface $token)
    {
        if ($this->decisionManager->decide($token, array('ROLE_STUDENT'))) {
            //user is author & this section of LA is not signed by student yet (means it's editable)
            if ($agreementSection->getLearningAgreement()->getStudent() === $user && !$agreementSection->getIsSignedByStudent())
                return true;
        }

        if ($this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
            if (!$agreementSection->getIsSignedBySending())
                return true;
        }

        return false;
    }

    /**
     * @param AgreementSection $agreementSection
     * @param User $user
     * @param TokenInterface $token
     * @return bool
     */
    private function canRemove(AgreementSection $agreementSection, User $user, TokenInterface $token)
    {
        if ($this->decisionManager->decide($token, array('ROLE_STUDENT'))) {
            //user is author & this section of LA is not signed by student yet (means it's editable)
            if ($agreementSection->getLearningAgreement()->getStudent() === $user && !$agreementSection->getIsSignedByStudent())
                return true;
        }

        if ($this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
            if (!$agreementSection->getIsSignedBySending())
                return true;
        }

        return false;
    }

    /**
     * @param AgreementSection $agreementSection
     * @param User $user
     * @param TokenInterface $token
     * @return bool
     */
    private function canView(AgreementSection $agreementSection, User $user, TokenInterface $token)
    {
        if ($this->decisionManager->decide($token, array('ROLE_STUDENT'))) {
            //user is author & this section of LA is not signed by student yet (means it's editable)
            if ($agreementSection->getLearningAgreement()->getStudent() === $user)
                return true;
        }

        if ($this->decisionManager->decide($token, array('ROLE_ADMIN'))) { // admin can view it if it's signed by student
            if ($agreementSection->getIsSignedByStudent())
                return true;
        }

        if ($this->decisionManager->decide($token, array('ROLE_RECEIVING_ACCOUNT'))) {
            if ($agreementSection->getLearningAgreement()->isResponsiblePersonLoggedIn($user) && $agreementSection->getIsSignedBySending())
                return true;

        }

        return false;
    }

    /**
     * @param AgreementSection $agreementSection
     * @param User $user
     * @return bool
     */
    private function canExport(AgreementSection $agreementSection, User $user)
    {
        if ($agreementSection->getStatus() === 'Approved By Both')
            return true;

        return false;

    }

    /**
     * @param AgreementSection $agreementSection
     * @param User $user
     * @return bool
     */
    private function canCreateDuring(AgreementSection $agreementSection, User $user, TokenInterface $token)
    {
        if ($this->decisionManager->decide($token, array('ROLE_STUDENT'))) {
            if ($agreementSection->getLearningAgreement()->getStudent() === $user)
                return true;
        }

        return false;

    }

    /**
     * @param AgreementSection $agreementSection
     * @param User $user
     * @return bool
     */
    private function canSign(AgreementSection $agreementSection, User $user, TokenInterface $token)
    {
        if ($this->decisionManager->decide($token, array('ROLE_STUDENT'))) {
            //user is author & this section of LA is not signed by student yet (means it's editable)
            if ($agreementSection->getLearningAgreement()->getStudent() === $user && !$agreementSection->getIsSignedByStudent())
                return true;
        }

        if ($this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
            if (!$agreementSection->getIsSignedBySending())
                return true;
        }

        if ($this->decisionManager->decide($token, array('ROLE_RECEIVING_ACCOUNT'))) {
            if ($agreementSection->getLearningAgreement()->isResponsiblePersonLoggedIn($user) && !$agreementSection->getIsSignedByReceiving())
                return true;
        }

        return false;

    }
}